const express = require("express");
const bodyParser = require("body-parser");
const path1 = 'D:/project/react/das/backend/views'

const path =  path1;

const app = express();
const cors = require('cors');
 const pool = require('../config/config');
 app.use(express.static(path));

 var corsOptions = {
  origin: "http://localhost:3000"
};

app.use(cors(corsOptions));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));



//middleware
 app.use(express.json());//req.body
 app.use(cors());

//Route//

app.get('/', function (req,res) {
  res.sendFile(path + "index.html");
});
  // Define a route for the search functionality
app.get('/landmark', async (req, res) => {
    try {
      const query = req.query.q;
      const results = await pool.query('SELECT * FROM landmark_4326_v5 WHERE name_eng ILIKE $1', [`%${query}%`]);
      res.json(results.rows);
    } catch (error) {
      console.error(error);
      res.status(500).json({ error: 'Internal server error' });
    }
  });
  app.get('/landmark1', async (req, res) => {
    try {
      const { Bldg_Long,Bldg_Lat } = req.query; // Get the gid value from the request query parameters
      const query = `
      SELECT *
      FROM landmark_4326_v5
      ORDER BY ST_Distance(geom, ST_SetSRID(ST_MakePoint($1, $2), 4326))
      LIMIT 1
    `;
    
      const results = await pool.query(query, [Bldg_Long,Bldg_Lat]); // Pass the gid value as a parameter in the query
      res.json(results.rows);
    } catch (error) {
      console.error(error);
      res.status(500).json({ error: 'Internal server error' });
    }
  });
  
  app.get('/resort', async (req, res) => {
    try {
      const query_SearchBy = req.query.q;
      const results = await pool.query('SELECT * FROM landmark_4326_v5 WHERE Fun_Type = 126');

      res.json(results.rows);
    } catch (error) {
      console.error(error);
      res.status(500).json({ error: 'Internal server error' });
    }
  });

  // Define an API endpoint for retrieving the feature based on coordinates
  app.get('/landmarks1', async (req, res) => {
    const { lat, lon } = req.query; // Get the latitude and longitude from the request query parameters
  
    try {
      // Construct the SQL query to select the nearest point feature
      const query = `
      SELECT * FROM landmark_4326_v5 WHERE Bldg_Long = $1 AND Bldg_L = $2 ORDER BY geom <-> ST_SetSRID(ST_MakePoint($1, $2), 4326)
      LIMIT 1
      `;
      const values = [lon, lat]; // Note the order of longitude (lon) and latitude (lat) in the query
  
      // Execute the query using the database connection pool
      const result = await pool.query(query, values);
  
      // Extract the feature information from the query result
      const feature = result.rows[0];
  
      // Return the feature information as a JSON response
      res.json(feature);
    } catch (error) {
      console.error('Error fetching feature information:', error);
      res.status(500).json({ error: 'Internal server error' });
    }
  });
  

    // Define a route for the search functionality
// app.get('/landmark1', async (req, res) => {
//   try {
//     const query = req.query.q;
//     const results = await pool.query('SELECT * FROM landmark_4326_v5  WHERE Bldg_Long = $1 AND  Bldg_Lat = $2');
//     res.json(results.rows);
//   } catch (error) {
//     console.error(error);
//     res.status(500).json({ error: 'Internal server error' });
//   }
// });





//   app.get("/api",async(req,res)=>{
//     try {
//         const { name } = req.query;
//         const landmark = await pool.query (
//             "SELECT name_eng,name_amh,Name_Local,region,City_Name,Bldg_Code FROM landmark_4326_v5 WHERE name_eng ILIKE $1",[`%${name}%`]);
//             //has sql Injection
//                 // `SELECT * FROM landmark_4326_v5 WHERE name_eng || '' || name_amh ILIKE '% ${name}%'`);

//                 res.send(landmark.rows.map(item => Object.values(item)));
//                 // res.send(Object.values(landmark.rows[0]));



            
//     } catch (err) {
//         console.error(err.message);
        
//     }
// });


// set port, listen for requests
const PORT = process.env.PORT || 4001;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});
//  app.listen(4001,()=>{console.log('server listen port 4001');
// })







// const express = require("express");
// const app = express();
// const cors = require('cors');
//  const pool = require('../config/config');

// //middleware
//  app.use(express.json());//req.body
//  app.use(cors());

// //Route//
 //select All from DB
//  app.get("/api",async(req,res)=>{
//     try {
//         const { name } = req.query;
//         const landmark = await pool.query (
//             "SELECT name_eng,name_amh,Name_Local,region,City_Name,Bldg_Code FROM landmark_4326_v5 WHERE name_amh  || name_eng ILIKE $1",[`%${name}%`]);
//             //has sql Injection
//                 // `SELECT * FROM landmark_4326_v5 WHERE name_eng || '' || name_amh ILIKE '% ${name}%'`);

//                 res.send(landmark.rows.map(item => Object.values(item)));
//                 // res.send(Object.values(landmark.rows[0]));



            
//     } catch (err) {
//         console.error(err.message);
        
//     }
// });

// app.get("/api",async(req,res)=>{
//     try {
//         // const allContacts = await pool.query("SELECT * FROM contact");
//         // res.json(allContacts.rows);   

//         const { name } = req.query;
//         const landmark = await pool.query (
//             "SELECT * FROM landmark_4326_v5");
//             //has sql Injection
//                 // `SELECT * FROM landmark_4326_v5 WHERE name_eng || '' || name_amh ILIKE '% ${name}%'`);

//             res.json(landmark.rows);
//     } catch (err) {
//         console.error(err.message);
        
//     }
// });




//  //select selected item
//  app.get("/api/:id",async(req,res)=>{
//     const {id} = req.params;
//     try {
//        const selectcontact = await pool.query("SELECT * FROM contact WHERE contact_id=$1",[id]);
//         res.json(selectcontact.rows[0]);
//      } catch (err) {
//         console.error(err.message);}
// });





//  //create
// app.post("/api",async(req,res)=>{
//     try {
//         const {name}= req.body;
//         const {phone_number}= req.body;
//        const newContact = await pool.query(
//             "INSERT INTO contact (name,phone_number) VALUES ($1,$2) RETURNING *",
//             [name,phone_number]

//        );
//         res.json(newContact)
//     } catch (err) {
//         console.error(err.message);
//     }
// });





//  //update
//  app.put("/api/:id",async(req,res)=>{
//     try {
//       const {id} = req.params; //where
//       const {name} = req.body; //set
//     const updatecontact = await pool.query(
//             "UPDATE contact SET name = $1 WHERE contact_id=$2",
//             [name,id]
//         );
//         res.json("contact was updated")
//      } catch (err) {
//         console.error(err.message);
//     }
// });





//  //delete

//  app.delete("/api/:id",async(req,res)=>{
//     try {
//       const {id} = req.params; //where
//       const updatecontact = await pool.query(
//             "DELETE FROM contact WHERE contact_id=$1",
//             [id]
//         );
//         res.json("contact was successfully deleted")
//     } catch (err) {
//         console.error(err.message);
//    }
// });
 


// app.listen(4001,()=>{console.log('server listen port 4001');
// })
