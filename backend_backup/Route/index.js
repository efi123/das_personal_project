//importing modules
const express = require('express')
const userController = require('../Controllers/index')
const { signup, login } = userController
const userAuth = require('../Middlewares/index')
const { Route } = require('react-router-dom')

const router = express.Router()
  // Define a route for the search functionality
  router.get('/landmark', async (req, res) => {
    try {
      const query = req.query.q;
      const results = await pool.query('SELECT * FROM landmark_4326_v5 WHERE name_eng ILIKE $1', [`%${query}%`]);
      res.json(results.rows);
    } catch (error) {
      console.error(error);
      res.status(500).json({ error: 'Internal server error' });
    }
  });
  router.get('/landmark1', async (req, res) => {
    try {
      const { Bldg_Long,Bldg_Lat } = req.query; // Get the gid value from the request query parameters
      const query = `
      SELECT *
      FROM landmark_4326_v5
      ORDER BY ST_Distance(geom, ST_SetSRID(ST_MakePoint($1, $2), 4326))
      LIMIT 1
    `;
    
      const results = await pool.query(query, [Bldg_Long,Bldg_Lat]); // Pass the gid value as a parameter in the query
      res.json(results.rows);
    } catch (error) {
      console.error(error);
      res.status(500).json({ error: 'Internal server error' });
    }
  });
  
  router.get('/resort', async (req, res) => {
    try {
      const query_SearchBy = req.query.q;
      const results = await pool.query('SELECT * FROM landmark_4326_v5 WHERE Fun_Type = 126');

      res.json(results.rows);
    } catch (error) {
      console.error(error);
      res.status(500).json({ error: 'Internal server error' });
    }
  });
//signup endpoint
//passing the middleware function to the signup
// router.post('/signup', userAuth.saveUser, signup)

// //login route
// router.post('/login', login )

module.exports = router
