const express = require("express");
const cors = require('cors');
 const db = require('./Models')
 const userRoutes = require ('./Route/index')

 
//setting up your port
const PORT = process.env.PORT || 4001   


//assigning the variable app to express
const app = express()

//middleware
app.use(express.json())
app.use(express.urlencoded({ extended: false }))






//routes for the user API
app.use('/', userRoutes)




//listening to server connection
app.listen(PORT, () => console.log(`Server is connected on ${PORT}`))
