const express = require("express");
const app = express();
const cors = require('cors');
 const pool = require('../models/index');




//signing a user up
//hashing users password before its saved to the database with bcrypt
const signup = async (req, res) => {
 try {
   const { userName, email, password } = req.body;
   const data = {
     userName,
     email,
     password: await bcrypt.hash(password, 10),
   };
   //saving the user
   const user = await User.create(data);

   //if user details is captured
   //generate token with the user's id and the secretKey in the env file
   // set cookie with the token generated
   if (user) {
     let token = jwt.sign({ id: user.id }, process.env.secretKey, {
       expiresIn: 1 * 24 * 60 * 60 * 1000,
     });

     res.cookie("jwt", token, { maxAge: 1 * 24 * 60 * 60, httpOnly: true });
     console.log("user", JSON.stringify(user, null, 2));
     console.log(token);
     //send users details
     return res.status(201).send(user);
   } else {
     return res.status(409).send("Details are not correct");
   }
 } catch (error) {
   console.log(error);
 }
};


//login authentication

const login = async (req, res) => {
 try {
const { email, password } = req.body;

   //find a user by their email
   const user = await User.findOne({
     where: {
     email: email
   } 
     
   });

   //if user email is found, compare password with bcrypt
   if (user) {
     const isSame = await bcrypt.compare(password, user.password);

     //if password is the same
      //generate token with the user's id and the secretKey in the env file

     if (isSame) {
       let token = jwt.sign({ id: user.id }, process.env.secretKey, {
         expiresIn: 1 * 24 * 60 * 60 * 1000,
       });

       //if password matches wit the one in the database
       //go ahead and generate a cookie for the user
       res.cookie("jwt", token, { maxAge: 1 * 24 * 60 * 60, httpOnly: true });
       console.log("user", JSON.stringify(user, null, 2));
       console.log(token);
       //send user data
       return res.status(201).send(user);
     } else {
       return res.status(401).send("Authentication failed");
     }
   } else {
     return res.status(401).send("Authentication failed");
   }
 } catch (error) {
   console.log(error);
 }
};





 app.get('/', function (req,res) {
    res.sendFile(path + "index.html");
  });
    // Define a route for the search functionality
  app.get('/landmark', async (req, res) => {
      try {
        const query = req.query.q;
        const results = await pool.query('SELECT * FROM landmark_4326_v5 ');
        res.json(results.rows);
      } catch (error) {
        console.error(error);
        res.status(500).json({ error: 'Internal server error' });
      }
    });
    app.get('/landmark1', async (req, res) => {
      try {
        const { Bldg_Long,Bldg_Lat } = req.query; // Get the gid value from the request query parameters
        const query = `
        SELECT *
        FROM landmark_4326_v5
        ORDER BY ST_Distance(geom, ST_SetSRID(ST_MakePoint($1, $2), 4326))
        LIMIT 1
      `;
      
        const results = await pool.query(query, [Bldg_Long,Bldg_Lat]); // Pass the gid value as a parameter in the query
        res.json(results.rows);
      } catch (error) {
        console.error(error);
        res.status(500).json({ error: 'Internal server error' });
      }
    });
    
    app.get('/resort', async (req, res) => {
      try {
        const query_SearchBy = req.query.q;
        const results = await pool.query('SELECT * FROM landmark_4326_v5 WHERE Fun_Type = 126');
  
        res.json(results.rows);
      } catch (error) {
        console.error(error);
        res.status(500).json({ error: 'Internal server error' });
      }
    });
  
    // Define an API endpoint for retrieving the feature based on coordinates
    app.get('/landmarks1', async (req, res) => {
      const { lat, lon } = req.query; // Get the latitude and longitude from the request query parameters
    
      try {
        // Construct the SQL query to select the nearest point feature
        const query = `
        SELECT * FROM landmark_4326_v5 WHERE Bldg_Long = $1 AND Bldg_L = $2 ORDER BY geom <-> ST_SetSRID(ST_MakePoint($1, $2), 4326)
        LIMIT 1
        `;
        const values = [lon, lat]; // Note the order of longitude (lon) and latitude (lat) in the query
    
        // Execute the query using the database connection pool
        const result = await pool.query(query, values);
    
        // Extract the feature information from the query result
        const feature = result.rows[0];
    
        // Return the feature information as a JSON response
        res.json(feature);
      } catch (error) {
        console.error('Error fetching feature information:', error);
        res.status(500).json({ error: 'Internal server error' });
      }
    });
    
  

