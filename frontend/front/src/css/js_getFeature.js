import React, { useEffect, useRef } from 'react';
import { Fragment } from 'react';
import TileLayer from 'ol/layer/Tile';
import TileWMS from 'ol/source/TileWMS';
import Overlay from 'ol/Overlay';
import { fromLonLat } from 'ol/proj';
import { Vector as VectorSource } from 'ol/source';
import { Vector as VectorLayer } from 'ol/layer';
import { get as getProjection } from 'ol/proj';
import View from 'ol/View';
import Map from 'ol/Map';
import './css/js.css'

const MyMapComponent = () => {

  useEffect(() => {
 
const wmsSource = new TileWMS({
  url: "http://localhost:8082/geoserver/Bishoftu_Das/wms",
  params: {LAYERS : 'Bishoftu_Das:lulc_v7'},
  serverType: 'geoserver',
  crossOrigin: 'anonymous',
});
const wmsSource1 = new TileWMS({
  url: "http://localhost:8082/geoserver/Bishoftu_Das/wms",
  params: {LAYERS : 'Bishoftu_Das:landmark_v7.2'},
  serverType: 'geoserver',
  crossOrigin: 'anonymous',
});

const wmsLayer = new TileLayer({
  source: wmsSource
});
const wmsLayer1 = new TileLayer({
  source: wmsSource1
});

const view = new View({
  center: fromLonLat([38.98341412184996, 8.763999146545743]),
  zoom: 11,
});

const map = new Map({
  layers: [wmsLayer,wmsLayer1],
  target: 'map',
  view: view,
});

map.on('singleclick', function (evt) {
  document.getElementById('info').innerHTML = '';
  const viewResolution = /** @type {number} */ (view.getResolution());
  const url = wmsSource1.getFeatureInfoUrl(
    evt.coordinate,
    viewResolution,
    'EPSG:3857',
    {'INFO_FORMAT': 'text/html'}
  );
  if (url) {
    fetch(url)
      .then((response) => response.text())
      .then((html) => {
        document.getElementById('info').innerHTML = html;
      });
  }
});

map.on('pointermove', function (evt) {
  if (evt.dragging) {
    return;
  }
  const data = wmsLayer.getData(evt.pixel);
  const hit = data && data[3] > 0; // transparent pixels have zero for data[3]
  map.getTargetElement().style.cursor = hit ? 'pointer' : '';
});

  });

  return (
    <Fragment>
      <div id="map" className="map"></div>
    <div id="info">&nbsp;</div>
    </Fragment>
  );
};

export default MyMapComponent;
