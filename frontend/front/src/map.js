import './css/side.css'
import Dir from './images/dir.jpg'
import Right_aroow from './images/right_arrow.PNG'
import Loc from './images/location.PNG'
import Ser from './images/service.PNG'
import Arow from './images/arrow.jfif'
import 'ol-layerswitcher/dist/ol-layerswitcher.css';
import LayerSwitcher from 'ol-layerswitcher';
import { BaseLayerOptions, GroupLayerOptions } from 'ol-layerswitcher';
import LayerGroup from 'ol/layer/Group';
import OSM from "ol/source/OSM";
import * as proj from 'ol/proj';
import Feature from 'ol/Feature';
import Point from 'ol/geom/Point';
import Overlay from 'ol/Overlay'
import { useEffect, useRef, useState } from 'react';
import { Map, View } from 'ol';
import TileLayer from 'ol/layer/Tile';
import {createStringXY} from 'ol/coordinate.js';

import {ScaleLine, defaults as defaultControls,MousePosition,FullScreen} from 'ol/control.js';
import TileWMS from 'ol/source/TileWMS';
import { fromLonLat } from 'ol/proj';
import { toLonLat} from 'ol/proj.js';
import {toStringHDMS} from 'ol/coordinate.js';
import * as bootstrap from 'bootstrap';
import Tile from 'ol/layer/Tile';
import { XYZ } from 'ol/source'
import {Stamen} from 'ol/source'

function Mymap() {
  const [name,setName]= useState("");
  console.log(name);



  //search onsubmit
  const onSubmitForm = async e=>{
    e.preventDefault();
    try{
const response = await fetch(`http://localhost:4001/api/?name=${name}`);
const parseResponse= await response.json();
console.log(parseResponse);
    }
    catch(err){
      console.error(err.message)
    }
  }
  const mousePositionControl = new MousePosition({
    coordinateFormat: createStringXY(4),
    projection: 'EPSG:4326',
   
  });

  //add map/Layer
  const mapRef = useRef(null);
  useEffect(() => {
    const map = new Map({
      controls: defaultControls().extend([mousePositionControl]),
      target: mapRef.current,
      layers: [
        new TileLayer({
          source: new OSM(),
          title:'osm',
          visible: false,
         }),
          new Tile({
            source: new XYZ({
              url: 'https://{a-c}.tile.openstreetmap.fr/hot/{z}/{x}/{y}.png',
            }),
            title: 'Humanitarian',
            visible: false,
          }),
          new Tile({
            // A layer must have a title to appear in the layerswitcher
            title: 'Satelite',
            // Again set this layer as a base layer
          
            visible: false,
            source: new Stamen({
                layer: 'terrain'
            })
          }),
        new TileLayer({
          source: new TileWMS({
            
            url: 'http://localhost:8080/geoserver/Das_Final/wms',
            params: { 'LAYERS': 'Das_Final:Das_Final', 'TILED': true },
            serverType: 'geoserver',
           
            type: 'base',
            group: 'base',
            visible: true,
            transition: 0
            }),
            title:'Bishoftu',
          } )
        
      ],
      
      view: new View({
        center: fromLonLat([38.965577549276546, 8.75323422342312]),
        
        zoom: 6,
    minZoom: 5,
    maxZoom: 19,
      }),
    });

   //layerSwitcher
    var layerSwitcher = new LayerSwitcher({
      reverse: true,
      activationMode:"click",
      groupSelectStyle: 'children'
     
    });
    map.addControl(layerSwitcher);



  //zoom Controller
    document.getElementById('zoom-out').onclick = function () {
      const view = map.getView();
      const zoom = view.getZoom();
      view.setZoom(zoom + 8);
    };
    document.getElementById('zoom-in').onclick = function () {
      const view = map.getView();
      const zoom = view.getZoom();
      view.setZoom(zoom - 8);
    };
    

// Add a click event listener to the map
map.on('singleclick', function(evt) {
  var coord = evt.coordinate;
  var transformed_coordinate = proj.transform(coord, "EPSG:900913", "EPSG:4326");
  console.log(transformed_coordinate);
  var test =('&nbsp&nbsp&nbsp&nbsp<button class="share">Share</button>')
  // alert("Clicked coordinates: " + transformed_coordinate[1]+ "" + ", " + "" + transformed_coordinate[0] );
  document.getElementById("coordinates").innerHTML = '<br>'+ transformed_coordinate  + test;
  var pointFeature = new Feature({
    geometry: new Point(transformed_coordinate)
    
  });
 
  });
   //mouse movemenet
   const projectionSelect = document.getElementById('projection');
   projectionSelect.addEventListener('change', function (event) {
     mousePositionControl.setProjection(event.target.value);
     
   });
//full screen
   var full_sc = new FullScreen({label:'F'});
   map.addControl(full_sc);










//Scale view
 var scaleLineMetric = new ScaleLine({
  units: ['metric'],
  target: document.getElementById("scaleline-metric")});
map.addControl(scaleLineMetric);
  });


   function direction (){
    var test ='Search Location'
    document.getElementById("test1").innerHTML = test;
   }
   function search_by_near (){
    alert("Boshoftu Maps search by near");
    var test ='test'
    document.getElementById("search_by_near").innerHTML = test;
   
   }
   function search_by_service (){
    alert("Boshoftu Maps search by service");
    var test ='test'
    document.getElementById("search_by_service").innerHTML = test;
   
   }
   function openNav() {
    document.getElementById("mySidebar").style.width = "250px";
    document.getElementById("main").style.marginLeft = "250px";
  }
  function closeNav1() {
    document.getElementById("direction").style.display = "none";
    document.getElementById("Search_near").style.display = "none";
    document.getElementById("Search_service").style.display = "none";
    document.getElementById("search").style.display = "none";
   document.getElementById("closeNav1").style.display = "none";
   document.getElementById("openNav1").style.display = "inline";
   document.getElementById("main").style.display = "none";
  }
  function openNav1() {  
    document.getElementById("direction").style.display = "inline";
    document.getElementById("Search_near").style.display = "inline";
    document.getElementById("Search_service").style.display = "inline";
    document.getElementById("search").style.display = "inline";
    document.getElementById("closeNav1").style.display = "inline";
    document.getElementById("openNav1").style.display = "none";
    document.getElementById("main").style.display = "inline";
    
  }
  function closeNav() {
    document.getElementById("mySidebar").style.width = "0";
    document.getElementById("main").style.marginLeft= "0";
  }

  return (
  <>  
 
  <div id="map-container" ref={mapRef} style={{ width: '100%', height: '560px' }} />
  <div className="container-input ">
  <div id="mySidebar" className="sidebar">
  <a href="javascript:void(0)" className="closebtn" onClick={closeNav} >×</a>
  <a href="#">SSGi</a>
  <a href="#">Services</a>
  <a href="#">Add place</a>
  <a href="#">Add Busieness</a>
</div>

<div id="main">
  <button className="openbtn"onClick={openNav}>☰ </button>  
</div>
 <form onSubmit={onSubmitForm} id="search">
  <input id="search"type="text"placeholder="Place,name address" onChange={e=>setName(e.target.value)}/>
  <svg fill="#000000" width="20px" height="20px" viewBox="0 0 1920 1920" xmlns="http://www.w3.org/2000/svg">
    <path d="M790.588 1468.235c-373.722 0-677.647-303.924-677.647-677.647 0-373.722 303.925-677.647 677.647-677.647 373.723 0 677.647 303.925 677.647 677.647 0 373.723-303.924 677.647-677.647 677.647Zm596.781-160.715c120.396-138.692 193.807-319.285 193.807-516.932C1581.176 354.748 1226.428 0 790.588 0S0 354.748 0 790.588s354.748 790.588 790.588 790.588c197.647 0 378.24-73.411 516.932-193.807l516.028 516.142 79.963-79.963-516.142-516.028Z" fill-rule="evenodd"></path>
</svg>
</form>

<img id="direction" title="Direction" className='icon1 icon' src={Dir} alt="Logo" style={{height: '35px',width:'35px'}} onClick={direction}/>
<img id="Search_near"title="Search near by"  className='icon2 icon'src={Loc} alt="Logo" style={{height: '35px',width:'35px'}}onClick={search_by_near}/>
<img id="Search_service"title="Search by service area"  className='icon3 icon' src={Ser} alt="Logo" style={{height: '35px',width:'35px'}}onClick={search_by_service}/>

<div id="closeNav1" >
  <button className=""onClick={closeNav1}><img id="closeIcon"title="collapse"  className='icon4 icon'  src={Arow} alt="Logo" style={{height: '20px',width:'20px'}}/>
 </button>  
</div>


<div id="openNav1" style={{display:'none'}} >
<button className=""onClick={openNav1}><img title="collapse"  className='icon4 icon'  src={Right_aroow} alt="Logo" style={{height: '10px',width:'10px'}}/>
 </button> 
</div>



</div>




  <a href='https://play.google.com/store/apps/details?id=net.gamiya.addis_abeba_ethiopia.offline.navigation'><button className='GetAPP'>Get the App</button></a>
    <button className='login'> <a href='./login'>Login</a></button>
<div className='location_share'>
<div id='coordinates'></div>

</div>
  <button id="zoom-out"className='zoom-out'>+</button>
  
    <button id="zoom-in"className='zoom-in'>-</button>
   
    <div className="bottomright">
      <div className='projection'>
      <div id='projection'></div>
      </div>
   <a href=""> Copyright © 2023 SSGI</a>
    </div>
    <div id="scaleline-metric"></div>
   
    
   
   
    </>
    )

}
export default Mymap;