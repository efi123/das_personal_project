import React, { useState } from 'react';
import "./css/js.css";

import 'ol/ol.css'; // OpenLayers CSS
import Map from 'ol/Map';
import Overlay from 'ol/Overlay';
import View from 'ol/View';
import TileLayer from 'ol/layer/Tile';
import XYZ from 'ol/source/XYZ';
import { transform } from 'ol/proj';
import { OSM } from "ol/source/OSM";
import Tile from "ol/layer/Tile";
import { fromLonLat } from "ol/proj";
import TileWMS from "ol/source/TileWMS";
import LayerSwitcher from "ol-layerswitcher";
import {MousePosition}  from "ol/control.js";

const MapComponent = () => {
  const [popupContent, setPopupContent] = useState('');

  var mapView = new View({
    center: fromLonLat([38.98, 8.76]),
    zoom: 14
});
var map = new Map({
    target: 'map',
    view: mapView
});



// var osmTile = new Tile({
//     title: 'Open Street Map',
//     visible: true,
//     source: new OSM()

// });
// map.addLayer(osmTile);

var lulc = new Tile({
    type: 'base',
    title: 'lulc',
    visible: true,
    source: new TileWMS({

        url: 'http://localhost:8082/geoserver/Bishoftu_Das/wms',
        params: { 'LAYERS': 'Bishoftu_Das:lulc_v7' },
        serverType: 'geoserver'
    })

});
map.addLayer(lulc);

var parcel = new Tile({
    title: 'parcel',
    visible: true,
    source: new TileWMS({
        url: 'http://localhost:8082/geoserver/Bishoftu_Das/wms',
        params: { 'LAYERS': 'Bishoftu_Das:parcel_v7' },
        serverType: 'geoserver',
        crossOrigin: 'anonymous'
    })
});
map.addLayer(parcel);

var nh = new Tile({
    title: 'nh',
    visible: true,
    source: new TileWMS({
        url: 'http://localhost:8082/geoserver/Bishoftu_Das/wms',
        params: { 'LAYERS': 'Bishoftu_Das:nh_aoi_v7.2' },
        serverType: 'geoserver',
        crossOrigin: 'anonymous'
    })
});
map.addLayer(nh);

var landmark = new Tile({
    title: 'Landmark',
    visible: true,
    source: new TileWMS({
        url: 'http://localhost:8082/geoserver/Bishoftu_Das/wms',
        params: { 'LAYERS': 'Bishoftu_Das:landmark_v7.2' },
        serverType: 'geoserver',
        crossOrigin: 'anonymous'
    })
});
map.addLayer(landmark);


var layerSwitcher = new LayerSwitcher({

    activationMood: 'click',
    startActive: false,
    groupSelectStyle: 'children'
});
map.addControl(layerSwitcher);

var mousePosition = new MousePosition({
    className: 'mousePosition',
    projection: 'EPSG:4326',
    coordinateFormat: function(coordinate) {
        return coordinate;
    }
});
map.addControl(mousePosition);

  // Create the overlay for the popup
  const popup = new Overlay({
    element: document.getElementById('popup'),
    autoPan: true,
    autoPanAnimation: {
      duration: 250,
    },
  });

  map.addOverlay(popup);

  // Event handler for map click
  map.on('click', function (evt) {
    setPopupContent('');
    const coordinate = evt.coordinate;
    popup.setPosition(coordinate);

    map.getLayers().forEach((layer) => {
      if (layer.get('title') === 'Landmark') {
        const viewResolution = map.getView().getResolution();
        const url = layer.getSource().getGetFeatureInfoUrl(
          evt.coordinate,
          viewResolution,
          'EPSG:3857',
          {
            INFO_FORMAT: 'application/json',
          }
        );

        if (url) {
          fetch(url)
            .then(function (response) {
              return response.json();
            })
            .then(function (json) {
              if (json.features.length > 0) {
                const feature = json.features[0];
                const properties = feature.properties;
                let content = '';
                for (const prop in properties) {
                  content += prop + ': ' + properties[prop] + '<br><br>';
                }
                setPopupContent(content);
              } else {
                setPopupContent('No information available.');
              }
            });
        }
      }
    });
  });

  return (
    <div>
    <div id="map" className="map"></div>
    <div id="popup" className="ol-popup">
        <a href="#" id="popup-closer" className="ol-popup-closer"></a>
        <div id="popup-content"></div>
    </div>
    </div>
  );
};

export default MapComponent;
