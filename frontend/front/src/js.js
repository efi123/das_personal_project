import 'ol-layerswitcher/dist/ol-layerswitcher.css';

import "./css/js.css";

import { Fragment } from "react";
import Map from "ol/Map";
import View from "ol/View";
import Overlay from "ol/Overlay";
import { ScaleLine, FullScreen } from "ol/control.js";
import TileLayer from "ol/layer/Tile";
import TileWMS from "ol/source/TileWMS";
import { fromLonLat } from "ol/proj";
import LayerSwitcher from "ol-layerswitcher";
import { XYZ } from "ol/source";
import { OSM } from "ol/source/OSM";
import Tile from "ol/layer/Tile";
import axios from "axios";
import { useRef, useEffect, useState, useCallback } from "react";
import Style from "ol/style/Style";
import Icon from "ol/style/Icon";
import Point from "ol/geom/Point";
import Feature from "ol/Feature";
import * as proj from "ol/proj";
import VectorLayer from "ol/layer/Vector";
import VectorSource from "ol/source/Vector";
import { FaBars } from "react-icons/fa";
import Logo from "./images/das-removebg.png";
import Dir from "./images/dir.PNG";
import Loc from "./images/location.PNG";
import Ser from "./images/service.PNG";
import share from "./images/share1.PNG";
import Bicycle from "./images/bi1.png";
import restorant from "./images/Restaurant.png";
import hotel from "./images/Hotel.png";
import guest_house from "./images/Gust House -Pension.png";
import pharmacy from "./images/Pharmacy.png";
import hospital from "./images/Hospital.png";
import Resort from "./images/resort.png";

import start from "./images/start_location.png";
import Foot from "./images/foot1.png";
import Location from "./images/location-icon.png";
import Right_aroow from "./images/right_arrow.PNG";
import bank from "./images/Bank.png";
import atm from "./images/ATM.png";
import supermarket from "./images/Super Market.png";
import police from "./images/Police Station.png";
import fuel from "./images/Fuel Station.png";
import university from "./images/University.png";
import or_church from "./images/Orthodox Church.png";
import pe_church from "./images/Church - Protestant.png";
import catholic from "./images/Catholic Church.png";
import mosque from "./images/Mosque.png";
import plus from "./images/plus1.png";
import flag from "./images/flag1.png";
import Arow from "./images/arrow-Icon.PNG";
import Bus from "./images/bus4.png";

function Testmap() {
  const mapRef = useRef(null);
  const contentRef = useRef(null);
  const popupOverlayRef = useRef(null);
  const [totalRows, setTotalRows] = useState(0);
  const [searchQuery, setSearchQuery] = useState("");
  const [searchResults, setSearchResults] = useState([]);
  const [searchResultsBy, setSearchResultsBy] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [markerPosition, setMarkerPosition] = useState(null);
  const [popupContent, setPopupContent] = useState('');

   ///////////////////////////////////////////////////////////////////////////////////////////////////////////
  //search onsubmit
  const fetchSearchResults = useCallback(async (query) => {
    if (!query) {
      // query is empty, do nothing
      return;
    }
    setIsLoading(true); // Set loading state to true
    try {
      const response = await axios.get(
        `http://localhost:4001/api/landmarks1?q=${query}`
      );
      setSearchResults(response.data);
    } catch (error) {
      console.error(error);
    } finally {
      setIsLoading(false); // Set loading state to false
    }
  }, []);
  useEffect(() => {
    // ... setup map ...
  }, [fetchSearchResults]);
  function handleSearchChange(e) {
    const input = e.target.value;
    console.log(input);
    setSearchQuery(input);
    fetchSearchResults(input);
  }

  useEffect(() => {
    // Create a new map instance
    const map = new Map({
      target: mapRef.current,
      layers: [],
      view: new View({
        center: fromLonLat([38.98341412184996, 8.763999146545743]),

        zoom: 13,
        // minZoom: 5,
        // maxZoom: 19,
      }),
    });

    //Create layers
    //   const OSM = new TileLayer({
    //   source: new OSM(),
    //   title: "osm",
    //   visible: true,
    // });
    const Humanitarian = new Tile({
      source: new XYZ({
        url: "https://{a-c}.tile.openstreetmap.fr/hot/{z}/{x}/{y}.png",
      }),
      title: "Humanitarian",
      visible: false,
    });

    const Satelite = new Tile({
      source: new XYZ({
        url: "https://mt1.google.com/vt/lyrs=s&x={x}&y={y}&z={z}",
      }),
      title: "Satelite",
      visible: false,
    });

    const Eth_aoi = new TileLayer({
      source: new TileWMS({
        url: "http://localhost:8082/geoserver/Bishoftu_Das/wms",
        params: { LAYERS: "Bishoftu_Das:eth_aoi", TILED: true },
        serverType: "geoserver",
        type: "base",
        visible: true,
        transition: 0,
      }),
      title: "ETH",
    });

    const Lulc = new TileLayer({
      source: new TileWMS({
        url: "http://localhost:8082/geoserver/Bishoftu_Das/wms",
        params: { LAYERS: "Bishoftu_Das:lulc_v7" },
        serverType: "geoserver",

        type: "base",
        visible: true,
        transition: 0,
      }),
      title: "LULC",
    });

    const Building = new TileLayer({
      source: new TileWMS({
        url: "http://localhost:8082/geoserver/Bishoftu_Das/wms",
        params: { LAYERS: "Bishoftu_Das:building_v7", TILED: true },
        serverType: "geoserver",

        type: "base",
        visible: true,
        transition: 0,
      }),
      title: "Bulding",
    });

    const NH = new TileLayer({
      source: new TileWMS({
        url: "http://localhost:8082/geoserver/Bishoftu_Das/wms",
        params: { LAYERS: "Bishoftu_Das:nh_aoi_v7.2", TILED: true },
        serverType: "geoserver",

        type: "base",
        visible: true,
        transition: 0,
      }),
      title: "NH",
    });

    const Prcl = new TileLayer({
      source: new TileWMS({
        url: "http://localhost:8082/geoserver/Bishoftu_Das/wms",
        params: { LAYERS: "Bishoftu_Das:parcel_v7", TILED: true },
        serverType: "geoserver",

        type: "base",
        visible: true,
        transition: 0,
      }),
      title: "Prcle",
    });

    const Road_Polygon = new TileLayer({
      source: new TileWMS({
        url: "http://localhost:8082/geoserver/Bishoftu_Das/wms",
        params: { LAYERS: "Bishoftu_Das:road_polygon_v7", TILED: true },
        serverType: "geoserver",

        type: "base",
        visible: true,
        transition: 0,
      }),
      title: "Road_Polygon",
    });

    const Road_Network = new TileLayer({
      source: new TileWMS({
        url: "http://localhost:8082/geoserver/Bishoftu_Das/wms",
        params: { LAYERS: "Bishoftu_Das:rd_network_v7.2", TILED: true },
        serverType: "geoserver",

        type: "base",
        visible: true,
        transition: 0,
      }),
      title: "Road_Network",
    });

    const Road_sign = new TileLayer({
      source: new TileWMS({
        url: "http://localhost:8082/geoserver/Bishoftu_Das/wms",
        params: { LAYERS: "Bishoftu_Das:road_signs_4326_v3", TILED: true },
        serverType: "geoserver",

        type: "base",
        visible: true,
        transition: 0,
      }),
      title: "Road_sign",
    });

    const Landmark = new TileLayer({
      source: new TileWMS({
        url: "http://localhost:8082/geoserver/Bishoftu_Das/wms",
        params: { LAYERS: "Bishoftu_Das:landmark_v7.2", TILED: true },
        serverType: "geoserver",

        type: "base",
        visible: true,
        transition: 0,
      }),
      title: "Landmark",
    });

    const driving_direction = new TileLayer({
      source: new TileWMS({
        url: "http://localhost:8082/geoserver/Bishoftu_Das/wms",
        params: {
          LAYERS: "Bishoftu_Das:driving_direction_4326_v3",
          TILED: true,
        },
        serverType: "geoserver",

        type: "base",
        visible: true,
        transition: 0,
      }),
      title: "driving_direction",
    });

    const Pa_Road = new TileLayer({
      source: new TileWMS({
        url: "http://localhost:8082/geoserver/Bishoftu_Das/wms",
        params: { LAYERS: "Bishoftu_Das:pa_roads_v2", TILED: true },
        serverType: "geoserver",

        type: "base",
        visible: true,
        transition: 0,
      }),
      title: "Pa_Road",
    });

    const Cities = new TileLayer({
      source: new TileWMS({
        url: "http://localhost:8082/geoserver/Bishoftu_Das/wms",
        params: { LAYERS: "Bishoftu_Das:cities_v7", TILED: true },
        serverType: "geoserver",

        visible: false,
        transition: 0,
      }),
      title: "Cities",
    });

    // Add layers to the map

    // map.addLayer(OSM);
   

    
    map.addLayer(Eth_aoi);
    map.addLayer(Lulc);
    map.addLayer(Building);
    map.addLayer(Prcl);
    map.addLayer(NH);
    map.addLayer(Pa_Road);
    map.addLayer(driving_direction);
    map.addLayer(Road_sign);
    map.addLayer(Road_Network);
    map.addLayer(Road_Polygon);
    map.addLayer(Landmark);
    map.addLayer(Cities);
    map.addLayer(Satelite);
    map.addLayer(Humanitarian);

    //full screen
    var full_sc = new FullScreen({
      target: document.getElementById("FullScreen"),
    });
    map.addControl(full_sc);

    //scaleLine
    var scaleLineMetric = new ScaleLine({
      bar: true,
      steps: 4,
      text: true,
      minWidth: 100,
      target: document.getElementById("scaleline_1"),
    });
    map.addControl(scaleLineMetric);

    // //layerSwitcher
    var layerSwitcher = new LayerSwitcher({
      activationMode: "click",
      startActive: false,
      groupSelectStyle: "children",
    });
    map.addControl(layerSwitcher);

    // Create a marker layer
    const markerLayer = new VectorLayer({
      source: new VectorSource(),
    });
    map.addLayer(markerLayer);
    let previousMarker = null; // Variable to store the previous marker
    map.on("singleclick", function (evt) {
          var coord = evt.coordinate;
          var transformed_coordinate = proj.transform(
        coord,
        "EPSG:900913",
        "EPSG:4326"
      );
      var share = document.createElement('div');
      share.innerHTML = '<button className="share" >share</button>';
      share.addEventListener('click', shareCoordinates);
      document.getElementById("coordinates").style.background = "white";
      document.getElementById("coordinates").innerHTML =transformed_coordinate;
      document.getElementById("coordinates").appendChild(share);


      //popup
      var view = map.getView();
      var zoom = view.getZoom() + 0.2;
      var duration = 500;
      var clickedCoord = evt.coordinate;

      if (previousMarker) {
        markerLayer.getSource().removeFeature(previousMarker);
      }
      const markerFeature = new Feature({
        geometry: new Point(clickedCoord),
      });
      const markerStyle = new Style({
        image: new Icon({
          src: Location, 
          anchor: [0.5, 1],
        }),
      });
      markerFeature.setStyle(markerStyle);
      markerLayer.getSource().addFeature(markerFeature);

      previousMarker = markerFeature;

      markerFeature.setStyle(markerStyle);
      var transformedCoord = proj.transform(
        clickedCoord,
        "EPSG:3857",
        view.getProjection()
      );
      view.animate({
        center: transformedCoord,
        duration: duration,
        zoom: zoom,
      });
      view.animate({
        center: transformedCoord,
        duration: duration,
        zoom: zoom,
      });
      var popupOverlay = new Overlay({
        element: document.getElementById("popup1"),
        positioning: "bottom-center",
        stopEvent: true,
        offset: [0, -20],
      });
     
      map.addOverlay(popupOverlay);
      document.getElementById("popup1-content").innerHTML= "<b><u>Bishoftu City</u></b> <br>" + 'Long = '+ transformed_coordinate[0].toFixed(5) + "<br/>"+ 'Lat = ' + transformed_coordinate[1].toFixed(5);
      // popupContent.innerHTML = "Bishoftu City <br>" +  transformed_coordinate[1] +  transformed_coordinate[0];


      document
        .getElementById("popup1-close")
        .addEventListener("click", function () {
          popupOverlay.setPosition(undefined);
        });

      
      popupOverlay.setPosition(clickedCoord);
     
    });
  }, []);
  function shareCoordinates() {
    var modal = document.getElementById("myModal");
    modal.style.display = "block";
    // Code for closing the modal
var span = document.getElementsByClassName("close")[0];
span.onclick = function() {
  var modal = document.getElementById("myModal");
  modal.style.display = "none";
};

window.onclick = function(event) {
  var modal = document.getElementById("myModal");
  if (event.target == modal) {
    modal.style.display = "none";
  }
};
  }

  function Search_By() {
    document.getElementById("Search_By").style.width = "480px";
    document.getElementById("totalRows").style.width = "180px";
  }
  function closeSearch() {
    document.getElementById("Search_By").style.width = "0";
    document.getElementById("totalRows").style.width = "0";
    document.getElementById("Search_Near_By").style.width = "0";
    document.getElementById("Search_By_Service_Area").style.width = "0";
    document.getElementById("Search_Near_By_input").style.width = "0";
    document.getElementById("Search_By_Service_Area_input").style.width = "0";

    document.getElementById("direction").style.width = "0";
    document.getElementById("search_by_service").style.width = "0";
    document.getElementById("search_by_near").style.width = "0";
    document.getElementById("Search_By_Area1").style.width = "0";
    document.getElementById("search-results-container-Searchby").style.width =
      "0";
  }

  function direction() {
    document.getElementById("direction").style.width = "490px";
  }
  function Search_Near_By() {
    document.getElementById("Search_Near_By").style.width = "490px";
    document.getElementById("Search_Near_By_input").style.width = "490px";
  }
  function Search_By_Service_Area() {
    document.getElementById("Search_By_Service_Area").style.width = "490px";
    document.getElementById("Search_By_Service_Area_input").style.width =
      "490px";
  }
  const [value, setValue] = useState(0);

  function updateTextInput(event) {
    var result = setValue(event.target.value);
    console.log(result);

    document.getElementById("range").innerHTML = value;
  }
function share_Location(){

}
async function CatagorySearch(value) {
  document.getElementById("search-results-container-Searchby").style.width =
    "480px";
  console.log(value);
  try {
    const response = await axios.get(
      `http://localhost:4001/api/landmarks?q=${value}`
    );
    setSearchResultsBy(response.data);
    setTotalRows(response.data.length); // Update the totalRows state

    console.log(response);
  } catch (error) {
    console.error(error);
  }
}
function showDirectionOnMap(long, lat) {
  console.log(long,lat);
        var coordinates = [parseFloat(long), parseFloat(lat)];
 console.log(coordinates);
       
       };
       function openNav() {
        document.getElementById("mySidebar").style.width = "330px";
        document.getElementById("main").style.marginLeft = "0";
      }; 
      function closeNav() {
        document.getElementById("mySidebar").style.width = "0";
        document.getElementById("main").style.marginLeft = "0";
      }
  return (
    <Fragment>
      <div ref={mapRef} className="map"></div>
      <div id="scaleline_1"></div>
      <div id="FullScreen"></div>
      <div className="main_page_top">
        <div className="main_page_top_Search_Box">
          <div className="sidebar-FaBars-icon">
            <FaBars  onClick={openNav} />
            &nbsp;&nbsp;<p className="vl"></p>
          </div>
          <div id="mySidebar" className="sidebar">
          <a href="javascript:void(0)" className="closebtn" onClick={closeNav}>
            ×
          </a>
        <div className="logo_side">
               <img  src={Logo}/>
        </div>
              
                    <hr />
          <a href="#">
            <h6>Add a Busieness</h6>
          </a>
          <a href="#">
            <h6>Add place</h6>
          </a>
          <a href="#">
            <h6>Close road</h6>
          </a>
          <br></br>
          Get Help
        </div>
          <div>
          <div className="input-box-container">
            <input
              type="text"
              placeholder="Place name, Address"
              value={searchQuery}
              onChange={handleSearchChange}
              onClick={Search_By}
            />
            </div>
                
          </div>





          <div className="main_page_top_icon">
            <img
              id="directionIcon"
              title="Direction"
              className="icon1 "
              src={Dir}
              alt="Logo"
              style={{ height: "50px", width: "50px" }}
              onClick={direction}
            />
            <img
              id="Search_by_nearIcon"
              title="Search near by"
              className="icon2 "
              src={Loc}
              alt="Logo"
              style={{ height: "50px", width: "50px" }}
              onClick={Search_Near_By}
            />
            <img
              id="Search_by_serviceIcon"
              title="Search by service area"
              className="icon3 "
              src={Ser}
              alt="Logo"
              style={{ height: "50px", width: "50px" }}
              onClick={Search_By_Service_Area}
            />
          </div>
        </div>
      </div>
      
      <div className="location_share">
        <div id="coordinates">
        <div id="myBtn">
        </div>

        </div>
        
      </div>

      <div className="Search_By" id="Search_By">
        <a
          href="#"
          className="closebtntab"
          onClick={closeSearch}
          title="close"
        >
          <div className="close-icon">&times;</div>
        </a>
        <h6 className="Search_by_title">Search By</h6>
        <div className="row" style={{ marginTop: "10px" }}>
          <div
            className="col 2 icon_search"
            onClick={() => CatagorySearch("126")}
          >
            <img
              title="Resort"
              src={Resort}
              style={{ width: "40px", height: "40px" }}
            />
          </div>
          <div
            className="col 2 icon_search "
            onClick={() => CatagorySearch("30")}
          >
            <img
              title="Restaurant"
              src={restorant}
              style={{ width: "40px", height: "40px" }}
            />
          </div>
          <div
            className="col 2 icon_search "
            onClick={() => CatagorySearch("27")}
          >
            <img
              title="Hotel"
              src={hotel}
              style={{ width: "40px", height: "40px" }}
            />
          </div>
          <div
            className="col 2 icon_search "
            onClick={() => CatagorySearch("165")}
          >
            <img
              title="Guest_house & Pension"
              src={guest_house}
              style={{ width: "40px", height: "40px" }}
            />
          </div>
          <div
            className="col 2 icon_search "
            onClick={() => CatagorySearch("69")}
          >
            <img
              title="Pharmacy"
              src={pharmacy}
              style={{ width: "40px", height: "40px" }}
            />
          </div>
          <div
            className="col 2 icon_search "
            onClick={() => CatagorySearch("65")}
          >
            <img
              title="Hospital"
              src={hospital}
              style={{ width: "40px", height: "40px" }}
            />
          </div>
        </div>
        <div className="row" style={{ marginTop: "20px" }}>
          <div
            className="col 2 icon_search "
            onClick={() => CatagorySearch("45")}
          >
            <img title="Bank" src={bank} />
          </div>
          <div
            className="col 2 icon_search "
            onClick={() => CatagorySearch("167")}
          >
            <img
              title="ATM"
              src={atm}
              style={{ width: "40px", height: "40px" }}
            />
          </div>
          <div
            className="col 2 icon_search "
            onClick={() => CatagorySearch("40")}
          >
            <img
              title="Supermarket"
              src={supermarket}
              style={{ width: "40px", height: "40px" }}
            />
          </div>
          <div
            className="col 2 icon_search"
            onClick={() => CatagorySearch("5")}
          >
            <img
              title="Police_station"
              src={police}
              style={{ width: "40px", height: "40px" }}
            />
          </div>
          <div
            className="col 2 icon_search "
            onClick={() => CatagorySearch("19")}
          >
            <img
              title="Fuel_Station"
              src={fuel}
              style={{ width: "40px", height: "40px" }}
            />
          </div>
          <div
            className="col 2 icon_search "
            onClick={() => CatagorySearch("58")}
          >
            <img
              title="University"
              src={university}
              style={{ width: "40px", height: "40px" }}
            />
          </div>
        </div>
        <div className="row" style={{ marginTop: "20px" }}>
          <div
            className="col 2 icon_search "
            onClick={() => CatagorySearch("90")}
          >
            <img
              title="Mosque"
              src={mosque}
              style={{ width: "40px", height: "40px" }}
            />
          </div>
          <div
            className="col 2 icon_search "
            onClick={() => CatagorySearch("87")}
          >
            <img
              title="Orthodox Church"
              src={or_church}
              style={{ width: "40px", height: "40px" }}
            />
          </div>
          <div
            className="col 2 icon_search "
            onClick={() => CatagorySearch("88")}
          >
            <img
              title="Protestant Church"
              src={pe_church}
              style={{ width: "40px", height: "40px" }}
            />
          </div>
          <div
            className="col 2 icon_search "
            onClick={() => CatagorySearch("89")}
          >
            <img
              title="Catholic"
              src={catholic}
              style={{ width: "40px", height: "40px" }}
            />
          </div>
          <div
            className="col 2 icon_search "
            onClick={() => CatagorySearch("")}
          ></div>
          <div
            className="col 2 icon_search "
            onClick={() => CatagorySearch("")}
          ></div>
        </div>
      </div>
      <div className="row container_No_found">
      {isLoading ? (
            <div className="Result-No-found">Loading...</div>
          ) : searchResults.length > 0 ? (
            <div className="search-results-container">
              {searchResults.slice(0, 2).map((result) => (
                <div key={result.nameEng} >
                    {result.nameEng}
                 <br/><br/>
                 
                </div>
              ))}
            </div>
          ) : searchQuery ? (
            <div className="Result-No-found">No landmark found</div>
          ) : null}
          </div>
      <div id="totalRows">Total Result found: {totalRows}</div>
      <div id="direction" className="direction">
        <a
          href="#"
          className="closebtntab"
          onClick={closeSearch}
          title="close"
        >
          <div className="close-icon">&times;</div>
        </a>
        <div className="row">
          <div className="icon-route-option">
            <img
              src={Bus}
              style={{ width: "30px", height: "20px", marginRight: "20px" }}
            />
            <img
              src={Bicycle}
              style={{ width: "30px", height: "20px", marginRight: "20px" }}
            />
            <img
              // onClick={searchFoot}
              src={Foot}
              style={{ width: "30px", height: "20px", marginRight: "20px" }}
            />
          </div>
          <br />
        </div>
        <div className="Direction_box" style={{ marginTop: "20px" }}>
          <div className="start_location">
            <img
              src={start}
              style={{ width: "20px", height: "20px", marginRight: "20px" }}
            />{" "}
            <input
              className="form-control"
              type="text"
              placeholder="Enter start location"
            />
          </div>
          <br />
          <div className="start_location">
            <img
              src={flag}
              style={{ width: "20px", height: "20px", marginRight: "20px" }}
            />{" "}
            <input
              className="form-control"
              type="text"
              placeholder="Enter start location"
            />
          </div>
          <br />

          <button className="btn  btn-Route">Find Route</button>
        </div>
      </div>

      <div className="Search_Near_By_input" id="Search_Near_By_input">
        <a
          href="#"
          className="closebtntab"
          onClick={closeSearch}
          title="close"
        >
          <div className="close-icon">&times;</div>
        </a>
        <h6 style={{ marginLeft: "40px" }}>Find Near By</h6>
        <br />
        <div className="start_location">
          <img
            src={start}
            style={{ width: "30px", height: "30px", marginLeft: "40px" }}
          />
          <div className="col-xs-2">
            <input
              className="form-control"
              id="ex1"
              placeholder="Enter Place"
              type="text"
            />
          </div>
        </div>
        <br />
        <div className="Search_Near_By_button">
          <button className="btn  btn-Search_Near_By">Search</button>
        </div>
        <br />
      </div>

      <div className="Search_By" id="Search_Near_By">
        <a
          href="#"
          className="closebtntab"
          onClick={closeSearch}
          title="close"
        >
          <div className="close-icon">&times;</div>
        </a>
        <h6 className="Search_by_title">Search_near_By</h6>
        <div className="row" style={{ marginTop: "10px" }}>
          <div
            className="col 2 icon_search"
            onClick={() => CatagorySearch("126")}
          >
            <img
              title="Resort"
              src={Resort}
              style={{ width: "40px", height: "40px" }}
            />
          </div>
          <div
            className="col 2 icon_search "
            onClick={() => CatagorySearch("30")}
          >
            <img
              title="Restaurant"
              src={restorant}
              style={{ width: "40px", height: "40px" }}
            />
          </div>
          <div
            className="col 2 icon_search "
            onClick={() => CatagorySearch("27")}
          >
            <img
              title="Hotel"
              src={hotel}
              style={{ width: "40px", height: "40px" }}
            />
          </div>
          <div
            className="col 2 icon_search "
            onClick={() => CatagorySearch("165")}
          >
            <img
              title="Guest_house & Pension"
              src={guest_house}
              style={{ width: "40px", height: "40px" }}
            />
          </div>
          <div
            className="col 2 icon_search "
            onClick={() => CatagorySearch("69")}
          >
            <img
              title="Pharmacy"
              src={pharmacy}
              style={{ width: "40px", height: "40px" }}
            />
          </div>
          <div
            className="col 2 icon_search "
            onClick={() => CatagorySearch("65")}
          >
            <img
              title="Hospital"
              src={hospital}
              style={{ width: "40px", height: "40px" }}
            />
          </div>
        </div>
        <div className="row" style={{ marginTop: "20px" }}>
          <div
            className="col 2 icon_search "
            onClick={() => CatagorySearch("45")}
          >
            <img title="Bank" src={bank} />
          </div>
          <div
            className="col 2 icon_search "
            onClick={() => CatagorySearch("167")}
          >
            <img
              title="ATM"
              src={atm}
              style={{ width: "40px", height: "40px" }}
            />
          </div>
          <div
            className="col 2 icon_search "
            onClick={() => CatagorySearch("40")}
          >
            <img
              title="Supermarket"
              src={supermarket}
              style={{ width: "40px", height: "40px" }}
            />
          </div>
          <div
            className="col 2 icon_search"
            onClick={() => CatagorySearch("5")}
          >
            <img
              title="Police_station"
              src={police}
              style={{ width: "40px", height: "40px" }}
            />
          </div>
          <div
            className="col 2 icon_search "
            onClick={() => CatagorySearch("19")}
          >
            <img
              title="Fuel_Station"
              src={fuel}
              style={{ width: "40px", height: "40px" }}
            />
          </div>
          <div
            className="col 2 icon_search "
            onClick={() => CatagorySearch("58")}
          >
            <img
              title="University"
              src={university}
              style={{ width: "40px", height: "40px" }}
            />
          </div>
        </div>
        <div className="row" style={{ marginTop: "20px" }}>
          <div
            className="col 2 icon_search "
            onClick={() => CatagorySearch("90")}
          >
            <img
              title="Mosque"
              src={mosque}
              style={{ width: "40px", height: "40px" }}
            />
          </div>
          <div
            className="col 2 icon_search "
            onClick={() => CatagorySearch("87")}
          >
            <img
              title="Orthodox Church"
              src={or_church}
              style={{ width: "40px", height: "40px" }}
            />
          </div>
          <div
            className="col 2 icon_search "
            onClick={() => CatagorySearch("88")}
          >
            <img
              title="Protestant Church"
              src={pe_church}
              style={{ width: "40px", height: "40px" }}
            />
          </div>
          <div
            className="col 2 icon_search "
            onClick={() => CatagorySearch("89")}
          >
            <img
              title="Catholic"
              src={catholic}
              style={{ width: "40px", height: "40px" }}
            />
          </div>
          <div
            className="col 2 icon_search "
            onClick={() => CatagorySearch("")}
          ></div>
          <div
            className="col 2 icon_search "
            onClick={() => CatagorySearch("")}
          ></div>
        </div>
      </div>

      <div className="Search_By_Service_Area_input"
        id="Search_By_Service_Area_input"
      >
        <a
          href="#"
          className="closebtntab"
          onClick={closeSearch}
          title="close"
        >
          <div className="close-icon">&times;</div>
        </a>
        <h6 style={{ marginLeft: "40px" }}>Search By Service Area</h6>
        <br />
        <div className="start_location">
          <img
            src={start}
            style={{ width: "30px", height: "30px", marginLeft: "40px" }}
          />
          <div className="col-xs-2">
            <input
              className="form-control"
              id="ex1"
              placeholder="Enter Place"
              type="text"
            />
          </div>
        </div>
        <br />

        <div style={{ width: "450px" }}>
          <input
            type="range"
            name="rangeInput"
            min={0}
            max={100}
            value={value}
            onChange={updateTextInput}
            style={{ width: "100%" }}
          />
        </div>
        <div id="range"></div>
        <div className="Search_Near_By_button">
          <button className="btn  btn-Search_Near_By">Search</button>
        </div>
        <br />
      </div>
      <div className="Search_By" id="Search_By_Service_Area">
        <a
          href="#"
          className="closebtntab"
          onClick={closeSearch}
          title="close"
        >
          <div className="close-icon">&times;</div>
        </a>
        <h6 className="Search_by_title">Search By Service Area</h6>
        <div className="row" style={{ marginTop: "10px" }}>
          <div
            className="col 2 icon_search"
            onClick={() => CatagorySearch("126")}
          >
            <img
              title="Resort"
              src={Resort}
              style={{ width: "40px", height: "40px" }}
            />
          </div>
          <div
            className="col 2 icon_search "
            onClick={() => CatagorySearch("30")}
          >
            <img
              title="Restaurant"
              src={restorant}
              style={{ width: "40px", height: "40px" }}
            />
          </div>
          <div
            className="col 2 icon_search "
            onClick={() => CatagorySearch("27")}
          >
            <img
              title="Hotel"
              src={hotel}
              style={{ width: "40px", height: "40px" }}
            />
          </div>
          <div
            className="col 2 icon_search "
            onClick={() => CatagorySearch("165")}
          >
            <img
              title="Guest_house & Pension"
              src={guest_house}
              style={{ width: "40px", height: "40px" }}
            />
          </div>
          <div
            className="col 2 icon_search "
            onClick={() => CatagorySearch("69")}
          >
            <img
              title="Pharmacy"
              src={pharmacy}
              style={{ width: "40px", height: "40px" }}
            />
          </div>
          <div
            className="col 2 icon_search "
            onClick={() => CatagorySearch("65")}
          >
            <img
              title="Hospital"
              src={hospital}
              style={{ width: "40px", height: "40px" }}
            />
          </div>
        </div>
        <div className="row" style={{ marginTop: "20px" }}>
          <div
            className="col 2 icon_search "
            onClick={() => CatagorySearch("45")}
          >
            <img title="Bank" src={bank} />
          </div>
          <div
            className="col 2 icon_search "
            onClick={() => CatagorySearch("167")}
          >
            <img
              title="ATM"
              src={atm}
              style={{ width: "40px", height: "40px" }}
            />
          </div>
          <div
            className="col 2 icon_search "
            onClick={() => CatagorySearch("40")}
          >
            <img
              title="Supermarket"
              src={supermarket}
              style={{ width: "40px", height: "40px" }}
            />
          </div>
          <div
            className="col 2 icon_search"
            onClick={() => CatagorySearch("5")}
          >
            <img
              title="Police_station"
              src={police}
              style={{ width: "40px", height: "40px" }}
            />
          </div>
          <div
            className="col 2 icon_search "
            onClick={() => CatagorySearch("19")}
          >
            <img
              title="Fuel_Station"
              src={fuel}
              style={{ width: "40px", height: "40px" }}
            />
          </div>
          <div
            className="col 2 icon_search "
            onClick={() => CatagorySearch("58")}
          >
            <img
              title="University"
              src={university}
              style={{ width: "40px", height: "40px" }}
            />
          </div>
        </div>
        <div className="row" style={{ marginTop: "20px" }}>
          <div
            className="col 2 icon_search "
            onClick={() => CatagorySearch("90")}
          >
            <img
              title="Mosque"
              src={mosque}
              style={{ width: "40px", height: "40px" }}
            />
          </div>
          <div
            className="col 2 icon_search "
            onClick={() => CatagorySearch("87")}
          >
            <img
              title="Orthodox Church"
              src={or_church}
              style={{ width: "40px", height: "40px" }}
            />
          </div>
          <div
            className="col 2 icon_search "
            onClick={() => CatagorySearch("88")}
          >
            <img
              title="Protestant Church"
              src={pe_church}
              style={{ width: "40px", height: "40px" }}
            />
          </div>
          <div
            className="col 2 icon_search "
            onClick={() => CatagorySearch("89")}
          >
            <img
              title="Catholic"
              src={catholic}
              style={{ width: "40px", height: "40px" }}
            />
          </div>
          <div
            className="col 2 icon_search "
            onClick={() => CatagorySearch("")}
          ></div>
          <div
            className="col 2 icon_search "
            onClick={() => CatagorySearch("")}
          ></div>
        </div>
      </div>


      <div
        className="search-results-container-Searchby"
        id="search-results-container-Searchby"
      >
        {searchResultsBy.map((landmark) => (
          <div key={landmark.gid} className="Searchby_body">
            {landmark.nameEng}&nbsp;/&nbsp;{landmark.nameAmh}/{landmark.region}
            <p>
              {landmark.nameEng} is found {landmark.region} region,{" "}
              {landmark.zone} zone ,{landmark.woreda} woreda ,{landmark.kebele}{" "}
              kebele ,in {landmark.city_name} city          
            </p>
            <div className="icon_location">
              <img
                id="directionIcon"
                className="icon_location1"
                title="Direction"
                src={share}
                alt="Logo"
                style={{ height: "45px", width: "50px" }}
              />

              <img
                id="directionIcon"
                className="icon_location2"
                title="Direction"
                src={Dir}
                alt="Logo"
                style={{ height: "40px", width: "40px" }}
                onClick={direction}
              />
              <img
                id="directionIcon"
                className="icon_location3"
                title="Direction"
                src={Loc}
                alt="Logo"
                style={{ height: "40px", width: "40px" }}
                
                onClick={() => showDirectionOnMap(landmark.long,landmark.lat)}
                />
            </div>
            <hr />
          </div>
        ))}
      </div>




      <div className="popup-container">
      <div id="popup1" className="ol-popup1">
        <div id="popup1-content"></div>
        <span id="popup1-close" className="popup1-close">
      
        </span>
      </div>
      </div>

<div id="myModal" className="modal">

  <div className="modal-content">
    <span className="close" ></span>
    <div className="row">
    <a href="#" className="fa fa-facebook"></a>
<a href="#" className="fa fa-twitter"></a>
<a href="#" className="fa fa-google"></a>
<a href="#" className="fa fa-linkedin"></a>
<a href="#" className="fa fa-skype"></a>
<a href="#" className="fa fa-instagram"></a>
<a href="#" className="fa fa-telegram" ></a>

</div>
  </div>

</div>
    </Fragment>
  );
}

export default Testmap;
