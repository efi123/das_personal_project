import React,{Fragment} from 'react';
import icon1 from './images/icon1.png';



const Login=()=>{
    
    return(
   <Fragment>
    <div className="portal">
<form action="">


        <div className="title">
            <h1>Login</h1>
           
        </div>
            <button id="google-signin">
            <img src={icon1} alt="Logo"width="30px" height="30px" />
               
                Login in with google
            </button>
            <span>or</span>
            <div className="input-field">
              
                <input type="text"id="username" placeholder="Email"required/>
               
            </div>
            <div className="input-field">
            
                <input type="password"id="password" placeholder="password"required/>
               
                <img id="show-hide-pass"src="" alt=""/>
            </div>
            <a href="http://"id="forgot-pass">Forgot Password</a>
            <button id="signin">Sign In</button>
            <p id="signup">
                Don't have an account?
                <a href="signup">Sign up here</a>
            </p>
    </form>
    </div>
</Fragment>
        
)
};
export default Login;