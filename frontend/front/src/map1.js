import './css/map.css'
import './css/map1.css'
import Dir from './images/dir.jpg'
import Loc from './images/location.PNG'
import Ser from './images/service.PNG'
import Menu from './images/menu_icon.jpg'
import Side from './side'
import 'ol-layerswitcher/dist/ol-layerswitcher.css';
import LayerSwitcher from 'ol-layerswitcher';
import { BaseLayerOptions, GroupLayerOptions } from 'ol-layerswitcher';
import LayerGroup from 'ol/layer/Group';
import OSM from "ol/source/OSM";
import * as proj from 'ol/proj';
import { useEffect, useRef } from 'react';
import { Map, View } from 'ol';
import TileLayer from 'ol/layer/Tile';


import TileWMS from 'ol/source/TileWMS';
import { fromLonLat } from 'ol/proj';

function Mymap1() {
  const mapRef = useRef(null);

  useEffect(() => {
    const map = new Map({
      target: mapRef.current,
      layers: [
        new TileLayer({
          title:'osm',
          type: 'base',
          group: 'base',
          visible: false,
          source: new OSM(),
          layer:'osm'
}),
        
        new TileLayer({
          source: new TileWMS({
            url: 'http://localhost:8080/geoserver/Das_Final/wms',
            params: { 'LAYERS': 'Das_Final:Das_Final', 'TILED': true },
            serverType: 'geoserver',
            type: 'base',
            visible: true,
            title:'Bishoftu',
            layer:'Bishoftu',
            group: 'base',
            transition: 0
          })
        } )
        
      ],
      
      view: new View({
        center: fromLonLat([38.965577549276546, 8.75323422342312]),
        zoom: 12,
      }),
    });

   
    var layerSwitcher = new LayerSwitcher({
      reverse: false,
      groupSelectStyle: 'group',
     
    });
    map.addControl(layerSwitcher);



  //zoom Controller
    document.getElementById('zoom-out').onclick = function () {
      const view = map.getView();
      const zoom = view.getZoom();
      view.setZoom(zoom + 8);
    };
    document.getElementById('zoom-in').onclick = function () {
      const view = map.getView();
      const zoom = view.getZoom();
      view.setZoom(zoom - 8);
    };
    

// Add a click event listener to the map
    map.on('singleclick', function(evt) {
      var coord = evt.coordinate;
      var transformed_coordinate = proj.transform(coord, "EPSG:900913", "EPSG:4326");
      // alert("Clicked coordinates: " + transformed_coordinate[1]+ "" + ", " + "" + transformed_coordinate[0] );
      document.getElementById("coordinates").innerHTML = transformed_coordinate;
       // Create a new div element to display the coordinates
  // var coordsDiv = document.createElement('div');
  // coordsDiv.className = 'coordinates';
  //  // Set the text content of the div to the transformed coordinates
  //  coordsDiv.textContent = 'Latitude: ' + transformed_coordinate[1] + ', Longitude: ' + transformed_coordinate[0];
  
  //  // Add the div to the map container
  //  map.getTargetElement().appendChild(coordsDiv);
  });
 
  });
 


 

  return (
  <>
  <div className='side_All'>
 
  <div id="map-container" ref={mapRef} style={{ width: '100%', height: '620px' }} />

  <div className="container-input">
  <img src={Menu} alt="Logo" />
  <input type="text" className="Search input" placeholder="Search Bishoftu Map" name="text" />
  <svg fill="#000000" width="20px" height="20px" viewBox="0 0 1920 1920" xmlns="http://www.w3.org/2000/svg">
    <path d="M790.588 1468.235c-373.722 0-677.647-303.924-677.647-677.647 0-373.722 303.925-677.647 677.647-677.647 373.723 0 677.647 303.925 677.647 677.647 0 373.723-303.924 677.647-677.647 677.647Zm596.781-160.715c120.396-138.692 193.807-319.285 193.807-516.932C1581.176 354.748 1226.428 0 790.588 0S0 354.748 0 790.588s354.748 790.588 790.588 790.588c197.647 0 378.24-73.411 516.932-193.807l516.028 516.142 79.963-79.963-516.142-516.028Z" fill-rule="evenodd"></path>
</svg>
<img src={Dir} alt="Logo" />
<img src={Loc} alt="Logo" />
<img src={Ser} alt="Logo" />

</div>
</div>


  <button className='GetAPP'>Get the App</button>
    <button className='login'> <a href='./login'>Login</a></button>
  <button id="zoom-out"className='zoom-out'>+</button>
    <button id="zoom-in"className='zoom-in'>-</button>
 <div id='coordinates'>ll</div>
    </>
    )

}
export default Mymap1;