import "./map.css";
import "./side.css";
import "./ol.css";
import share from "./share1.PNG";
import Resort from "./resort.png";
import Dir from "./dir.PNG";
import Logo from "./das.jpg";
import Bus from "./bus4.png";
import Bicycle from "./bi1.png";
import restorant from "./Restaurant.png";
import hotel from "./Hotel.png";
import guest_house from "./Gust House -Pension.png";
import pharmacy from "./Pharmacy.png";
import hospital from "./Hospital.png";
import start from "./start_location.png";
import Foot from "./foot1.png";
import Right_aroow from "./right_arrow.PNG";
import bank from "./Bank.png";
import atm from "./ATM.png";
import supermarket from "./Super Market.png";
import police from "./Police Station.png";
import fuel from "./Fuel Station.png";
import university from "./University.png";
import or_church from "./Orthodox Church.png";
import pe_church from "./Church - Protestant.png";
import catholic from "./Catholic Church.png";
import mosque from "./Mosque.png";
import Loc from "./location.PNG";
import Ser from "./service.PNG";
import plus from "./plus1.png";
import flag from "./flag1.png";
import Arow from "./arrow-Icon.PNG";
import "ol-layerswitcher/dist/ol-layerswitcher.css";
import LayerSwitcher from "ol-layerswitcher";
import OSM from "ol/source/OSM";
import * as proj from "ol/proj";
import { useEffect, useRef, useState, useCallback } from "react";
import { Map, View } from "ol";
import TileLayer from "ol/layer/Tile";
import { FaSearch, FaBars } from "react-icons/fa";
import {ScaleLine,FullScreen,} from "ol/control.js";
import TileWMS from "ol/source/TileWMS";
import { fromLonLat } from "ol/proj";
import Tile from "ol/layer/Tile";
import { XYZ } from "ol/source";
import { Stamen } from "ol/source";
import { Fragment } from "react";
import axios from "axios";
import Feature from 'ol/Feature';
import Point from 'ol/geom/Point';
import VectorLayer from 'ol/layer/Vector';
import VectorSource from 'ol/source/Vector';





function Testmap() {
  const [totalRows, setTotalRows] = useState(0);
  const [searchQuery, setSearchQuery] = useState("");
  const [searchResults, setSearchResults] = useState([]);
  const [searchResultsBy, setSearchResultsBy] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [markerPosition, setMarkerPosition] = useState(null);

  const mapRef = useRef(null);

  ///////////////////////////////////////////////////////////////////////////////////////////////////////////
  //search onsubmit
  const fetchSearchResults = useCallback(async (query) => {
    if (!query) {
      // query is empty, do nothing
      return;
    }
    setIsLoading(true); // Set loading state to true
    try {
      const response = await axios.get(
        `http://localhost:4001/api/landmark1?q=${query}`
      );
      setSearchResults(response.data);
    } catch (error) {
      console.error(error);
    } finally {
      setIsLoading(false); // Set loading state to false
    }
  }, []);
  useEffect(() => {
    // ... setup map ...
  }, [fetchSearchResults]);
  function handleSearchChange(e) {
    const input = e.target.value;
    console.log(input);
    setSearchQuery(input);
    fetchSearchResults(input);
  }


  ///////////////////////////////////////////////////////////////////////////////////////////////////////////
  //add map/Layer

    const map = new Map({
      target: "map-container",
      layers: [
        new TileLayer({
          source: new OSM(),
          title: "osm",
          visible: false,
        }),
        new Tile({
          source: new XYZ({
            url: "https://{a-c}.tile.openstreetmap.fr/hot/{z}/{x}/{y}.png",
          }),
          title: "Humanitarian",
          visible: false,
        }),
        new Tile({
          // A layer must have a title to appear in the layerswitcher
          title: "Satelite",
          // Again set this layer as a base layer

          visible: false,
          source: new Stamen({
            layer: "terrain",
          }),
        }),
        new TileLayer({
          source: new TileWMS({
            url: "http://localhost:8080/geoserver/Das_Final/wms",
            params: { LAYERS: "Das_Final:Das_Final", TILED: true },
            serverType: "geoserver",

            type: "base",
            visible: true,
            transition: 0,
          }),
          title: "Bishoftu",
        }),
      ],

      view: new View({
        center: fromLonLat([38.98341412184996, 8.763999146545743]),

        zoom: 13,
        // minZoom: 5,
        // maxZoom: 19,
      }),
    });
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////

//layerSwitcher
    // var layerSwitcher = new LayerSwitcher({
    //   activationMode: "click",
    //   groupSelectStyle: "children",
    // });
    // map.addControl(layerSwitcher);

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////
    //full screen
    var full_sc = new FullScreen({
      target: document.getElementById("FullScreen"),
    });
    map.addControl(full_sc);
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////
    //click event
    map.on("click", function handleMapClick(evt) {

      console.log(evt.target);
      var view = map.getView();
      // Define the zoom level and duration of the animation
      var zoom = view.getZoom() + 0.2;
      var duration = 500; // in milliseconds
      // Get the clicked coordinates and transform them to the map's projection
      var clickedCoord = evt.coordinate;

      var transformedCoord = proj.transform(
        clickedCoord,
        "EPSG:3857",
        view.getProjection()
      );

      // Animate the view to the new zoom level centered on the clicked point
      view.animate({
        center: transformedCoord,
        duration: duration,
        zoom: zoom,
      });
      var coord = evt.coordinate;
      var transformed_coordinate = proj.transform(
        coord,
        "EPSG:900913",
        "EPSG:4326"
      );
      var share = '<div><button class="share">share</button></div>';
      document.getElementById("coordinates").style.background = "white";
      document.getElementById("coordinates").innerHTML =
        transformed_coordinate + share;
    });

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////
    //Scale view
    var scaleLineMetric = new ScaleLine({
      units: ["metric"],
      target: document.getElementById("scaleline-metric"),
    });
    map.addControl(scaleLineMetric);
 

  ///////////////////////////////////////////////////////////////////////////////////////////////////////////
  //functions
  function Search_By() {
    document.getElementById("Search_By").style.width = "480px";
    document.getElementById("totalRows").style.width = "180px";
  }

  function direction() {
    document.getElementById("direction").style.width = "420px";
  }
  function search_by_near() {
    document.getElementById("search_by_near").style.width = "420px";
    document.getElementById("Search_By_Area1").style.width = "420px";
    document.getElementById("Search_By_Area1").style.marginTop = "140px";
  }
  function search_by_service() {
    document.getElementById("search_by_service").style.width = "420px";
    document.getElementById("Search_By_Area1").style.width = "420px";
    document.getElementById("Search_By_Area1").style.marginTop = "140px";
  }
  function openNav() {
    document.getElementById("mySidebar").style.width = "250px";
    document.getElementById("main").style.marginLeft = "0";
  }
  function closeNav1() {
    document.getElementById("directionIcon").style.display = "none";
    document.getElementById("Search_by_nearIcon").style.display = "none";
    document.getElementById("Search_by_serviceIcon").style.display = "none";
    document.getElementById("closeNav1").style.display = "none";
    document.getElementById("openNav1").style.display = "inline";
    document.getElementById("sidebar-icon").style.display = "none";
    document.getElementById("search-box").style.display = "none";
  }
  function openNav1() {
    document.getElementById("directionIcon").style.display = "inline";
    document.getElementById("Search_by_nearIcon").style.display = "inline";
    document.getElementById("Search_by_serviceIcon").style.display = "inline";
    document.getElementById("closeNav1").style.display = "inline";
    document.getElementById("openNav1").style.display = "none";
    document.getElementById("sidebar-icon").style.display = "inline";
    document.getElementById("search-box").style.display = "inline";
  }
  function closeNav() {
    document.getElementById("mySidebar").style.width = "0";
    document.getElementById("main").style.marginLeft = "0";
  }
  function closeSearch() {
    document.getElementById("Search_By").style.width = "0";
    document.getElementById("direction").style.width = "0";
    document.getElementById("search_by_service").style.width = "0";
    document.getElementById("search_by_near").style.width = "0";
    document.getElementById("Search_By_Area1").style.width = "0";
    document.getElementById("search-results-container-Searchby").style.width =
      "0";
  }
  function searchFoot() {}


  async function CatagorySearch(value) {
    document.getElementById("search-results-container-Searchby").style.width =
      "480px";
    console.log(value);
    try {
      const response = await axios.get(
        `http://localhost:4001/api/landmarks?q=${value}`
      );
      setSearchResultsBy(response.data);
      setTotalRows(response.data.length); // Update the totalRows state

      console.log(response);
    } catch (error) {
      console.error(error);
    }
  }
  
  // Add this before the useEffect hook


  
function showDirectionOnMap(long, lat) {
 console.log(long,lat);
       var coordinates = [parseFloat(long), parseFloat(lat)];
console.log(coordinates);
      
      };
   

  

  return (
    <Fragment>
      {/* <div id="map-container"ref={mapRef}/> */}
      <div id="map-container" ></div>

      <div className="Search_By" id="Search_By">
        <a
          href="javascript:void(0)"
          className="closebtntab"
          onClick={closeSearch}
          title="close"

        >
          <div class="close-icon">&times;</div>
        </a>
        <h6 className="Search_by_title">Search By</h6>
        <div className="row" style={{ marginTop: "10px" }}>
          <div
            className="col 2 icon_search"
            onClick={() => CatagorySearch("126")}
          >
            <img
              title="Resort"
              src={Resort}
              style={{ width: "40px", height: "40px" }}
            />
          </div>
          <div
            className="col 2 icon_search "
            onClick={() => CatagorySearch("30")}
          >
            <img
              title="Restaurant"
              src={restorant}
              style={{ width: "40px", height: "40px" }}
            />
          </div>
          <div
            className="col 2 icon_search "
            onClick={() => CatagorySearch("27")}
          >
            <img
              title="Hotel"
              src={hotel}
              style={{ width: "40px", height: "40px" }}
            />
          </div>
          <div
            className="col 2 icon_search "
            onClick={() => CatagorySearch("165")}
          >
            <img
              title="Guest_house & Pension"
              src={guest_house}
              style={{ width: "40px", height: "40px" }}
            />
          </div>
          <div
            className="col 2 icon_search "
            onClick={() => CatagorySearch("69")}
          >
            <img
              title="Pharmacy"
              src={pharmacy}
              style={{ width: "40px", height: "40px" }}
            />
          </div>
          <div
            className="col 2 icon_search "
            onClick={() => CatagorySearch("65")}
          >
            <img
              title="Hospital"
              src={hospital}
              style={{ width: "40px", height: "40px" }}
            />
          </div>
        </div>
        <div className="row" style={{ marginTop: "20px" }}>
          <div
            className="col 2 icon_search "
            onClick={() => CatagorySearch("45")}
          >
            <img title="Bank" src={bank} />
          </div>
          <div
            className="col 2 icon_search "
            onClick={() => CatagorySearch("167")}
          >
            <img
              title="ATM"
              src={atm}
              style={{ width: "40px", height: "40px" }}
            />
          </div>
          <div
            className="col 2 icon_search "
            onClick={() => CatagorySearch("40")}
          >
            <img
              title="Supermarket"
              src={supermarket}
              style={{ width: "40px", height: "40px" }}
            />
          </div>
          <div
            className="col 2 icon_search"
            onClick={() => CatagorySearch("5")}
          >
            <img
              title="Police_station"
              src={police}
              style={{ width: "40px", height: "40px" }}
            />
          </div>
          <div
            className="col 2 icon_search "
            onClick={() => CatagorySearch("19")}
          >
            <img
              title="Fuel_Station"
              src={fuel}
              style={{ width: "40px", height: "40px" }}
            />
          </div>
          <div
            className="col 2 icon_search "
            onClick={() => CatagorySearch("58")}
          >
            <img
              title="University"
              src={university}
              style={{ width: "40px", height: "40px" }}
            />
          </div>
        </div>
        <div className="row" style={{ marginTop: "20px" }}>
          <div
            className="col 2 icon_search "
            onClick={() => CatagorySearch("90")}
          >
            <img
              title="Mosque"
              src={mosque}
              style={{ width: "40px", height: "40px" }}
            />
          </div>
          <div
            className="col 2 icon_search "
            onClick={() => CatagorySearch("87")}
          >
            <img
              title="Orthodox Church"
              src={or_church}
              style={{ width: "40px", height: "40px" }}
            />
          </div>
          <div
            className="col 2 icon_search "
            onClick={() => CatagorySearch("88")}
          >
            <img
              title="Protestant Church"
              src={pe_church}
              style={{ width: "40px", height: "40px" }}
            />
          </div>
          <div
            className="col 2 icon_search "
            onClick={() => CatagorySearch("89")}
          >
            <img
              title="Catholic"
              src={catholic}
              style={{ width: "40px", height: "40px" }}
            />
          </div>
          <div
            className="col 2 icon_search "
            onClick={() => CatagorySearch("")}
          ></div>
          <div
            className="col 2 icon_search "
            onClick={() => CatagorySearch("")}
          ></div>
        </div>
      </div>
      <div id="totalRows">Total Result found: {totalRows}</div>
      <div
        className="search-results-container-Searchby"
        id="search-results-container-Searchby"
      >
        {searchResultsBy.map((landmark) => (
          <div key={landmark.gid} className="Searchby_body">
            {landmark.nameEng}&nbsp;/&nbsp;{landmark.nameAmh}/{landmark.region}
            <p>
              {landmark.nameEng} is found {landmark.region} region,{" "}
              {landmark.zone} zone ,{landmark.woreda} woreda ,{landmark.kebele}{" "}
              kebele ,in {landmark.city_name} city          
            </p>
            <div className="icon_location">
              <img
                id="directionIcon"
                className="icon_location1"
                title="Direction"
                src={share}
                alt="Logo"
                style={{ height: "45px", width: "50px" }}
              />

              <img
                id="directionIcon"
                className="icon_location2"
                title="Direction"
                src={Dir}
                alt="Logo"
                style={{ height: "40px", width: "40px" }}
                onClick={direction}
              />
              <img
                id="directionIcon"
                className="icon_location3"
                title="Direction"
                src={Loc}
                alt="Logo"
                style={{ height: "40px", width: "40px" }}
                
                onClick={() => showDirectionOnMap(landmark.long,landmark.lat)}
                />
            </div>
            <hr />
          </div>
        ))}
      </div>
      <div id="direction" className="direction">
        <a
          href="javascript:void(0)"
          className="closebtntab"
          onClick={closeSearch}
          title="close"
        >
          <div class="close-icon">&times;</div>
        </a>
        <div className="row">
          <div className="col 2 bus">
            <img
              src={Bus}
              style={{ width: "40px", height: "40px", marginRight: "20px" }}
            />
            <img
              src={Bicycle}
              style={{ width: "40px", height: "40px", marginRight: "20px" }}
            />
            <img
              onClick={searchFoot}
              src={Foot}
              style={{ width: "40px", height: "40px", marginRight: "20px" }}
            />
          </div>
          <br />
          <br />
        </div>
        <div className="Direction_box" style={{ marginTop: "20px" }}>
          <div className="start_location">
            <img
              src={start}
              style={{ width: "40px", height: "40px", marginRight: "20px" }}
            />{" "}
            <input
              className="form-control"
              type="text"
              placeholder="Enter start location"
            />
          </div>
          <br />
          <div className="start_location">
            <img
              src={flag}
              style={{ width: "40px", height: "40px", marginRight: "20px" }}
            />{" "}
            <input
              className="form-control"
              type="text"
              placeholder="Enter start location"
            />
          </div>
          <br />
          <div className="start_location">
            <img
              src={plus}
              style={{ width: "40px", height: "40px", marginRight: "20px" }}
            />{" "}
            Add Distination
          </div>
          <br />

          <button className="btn btn_findRoute ">Find Route</button>
        </div>
      </div>

      <div id="search_by_near" className="search_by_near">
        <a
          href="javascript:void(0)"
          className="closebtntab"
          onClick={closeSearch}
          title="close"
        >
          <div class="close-icon">&times;</div>
        </a>
        <h6 className="Find_place_box_title">Find nearby</h6>
        <div className="Find_place_box" style={{ marginTop: "20px" }}>
          <div className="start_location">
            <img
              src={start}
              style={{ width: "40px", height: "40px", marginRight: "20px" }}
            />{" "}
            <input
              className="form-control"
              type="text"
              placeholder="Enter start location"
            />
          </div>
          <br />
          <button>Search</button>
        </div>
      </div>

      <div id="search_by_service" className="search_by_Area">
        <a
          href="javascript:void(0)"
          className="closebtntab"
          onClick={closeSearch}
          title="close"
        >
          <div class="close-icon">&times;</div>
        </a>
        <h6 className="search_by_Area_title">Search by Area</h6>
        <div className="Find_place_box" style={{ marginTop: "20px" }}>
          <div className="start_location">
            <img
              src={start}
              style={{ width: "40px", height: "40px", marginRight: "20px" }}
            />{" "}
            <input
              className="form-control"
              type="text"
              placeholder="Enter start location"
            />
          </div>
          <br />
          <button className="btn">Search</button>
        </div>
      </div>
      <div className="Search_By_Area1" id="Search_By_Area1">
        <a
          href="javascript:void(0)"
          className="closebtntab"
          onClick={closeSearch}
          title="close"
        >
          <div class="close-icon">&times;</div>
        </a>
        <div className="row" style={{ marginTop: "40px" }}>
          <div className="col 2 icon_search">
            <img
              title="Resort"
              src={Resort}
              style={{ width: "40px", height: "40px" }}
            />
          </div>
          <div className="col 2 icon_search">
            <img
              title="Restorant"
              src={restorant}
              style={{ width: "40px", height: "40px" }}
            />
          </div>
          <div className="col 2 icon_search ">
            <img
              title="Hotel"
              src={hotel}
              style={{ width: "40px", height: "40px" }}
            />
          </div>
          <div className="col 2 icon_search ">
            <img
              title="Guest_house & Pension"
              src={guest_house}
              style={{ width: "40px", height: "40px" }}
            />
          </div>
          <div className="col 2 icon_search ">
            <img
              title="Pharmacy"
              src={pharmacy}
              style={{ width: "40px", height: "40px" }}
            />
          </div>
          <div className="col 2 icon_search ">
            <img
              title="Hospital"
              src={hospital}
              style={{ width: "40px", height: "40px" }}
            />
          </div>
        </div>
        <div className="row" style={{ marginTop: "40px" }}>
          <div className="col 2 icon_search ">
            <img title="Bank" src={bank} />
          </div>
          <div className="col 2 icon_search ">
            <img
              title="ATM"
              src={atm}
              style={{ width: "40px", height: "40px" }}
            />
          </div>
          <div className="col 2 icon_search ">
            <img
              title="Supermarket"
              src={supermarket}
              style={{ width: "40px", height: "40px" }}
            />
          </div>
          <div className="col 2 ">
            <img
              title="Police_station"
              src={police}
              style={{ width: "40px", height: "40px" }}
            />
          </div>
          <div className="col 2 icon_search ">
            <img
              title="Fuel_Station"
              src={fuel}
              style={{ width: "40px", height: "40px" }}
            />
          </div>
          <div className="col 2 icon_search ">
            <img
              title="University"
              src={university}
              style={{ width: "40px", height: "40px" }}
            />
          </div>
        </div>
        <div className="row" style={{ marginTop: "40px" }}>
          <div className="col 2 icon_search ">
            <img
              title="Mosque"
              src={mosque}
              style={{ width: "40px", height: "40px" }}
            />
          </div>
          <div className="col 2 icon_search ">
            <img
              title="Orthodox Church"
              src={or_church}
              style={{ width: "40px", height: "40px" }}
            />
          </div>
          <div className="col 2 icon_search ">
            <img
              title="Protestant Church"
              src={pe_church}
              style={{ width: "40px", height: "40px" }}
            />
          </div>
          <div className="col 2 icon_search ">
            <img
              title="Catholic"
              src={catholic}
              style={{ width: "40px", height: "40px" }}
            />
          </div>
          <div className="col 2 icon_search "></div>
          <div className="col 2 icon_search "></div>
        </div>
      </div>

      <div className="container-input ">
        <div id="mySidebar" className="sidebar">
          <a href="javascript:void(0)" className="closebtn" onClick={closeNav}>
            ×
          </a>
          <h2>
            <a href="#">
              {" "}
              <h6>
                <img
                  src={Logo}
                  style={{ width: "40px", height: "40px", marginLeft: "-40px" }}
                />
                &nbsp; Bishoftu Map
              </h6>
            </a>
          </h2>
          <hr />
          <a href="#">
            <h6>Add a Busieness</h6>
          </a>
          <a href="#">
            <h6>Add place</h6>
          </a>
          <a href="#">
            <h6>Close road</h6>
          </a>
          <br></br>
          Get Help
        </div>

        <div id="main">
          <button className="openbtn" onClick={openNav}>
            <div className="sidebar-icon" id="sidebar-icon">
              <FaBars onClick={openNav} />
            </div>
          </button>
        </div>

        <div>
          <div>
            <div className="input-box-container">
              <div className="search-box" id="search-box">
                <input
                  type="text"
                  className="search-box"
                  placeholder="Search"
                  value={searchQuery}
                  onChange={handleSearchChange}
                  onClick={Search_By}
                />
              </div>
            </div>
          </div>

          {isLoading ? (
            <div>Loading...</div>
          ) : searchResults.length > 0 ? (
            <div className="search-results-container">
              {searchResults.slice(0, 2).map((result) => (
                <div key={result.name_eng}>
                  <a href={`${result.name_eng}`}>
                    {result.name_eng}&nbsp;/&nbsp;{result.name_amh}
                  </a>
                  <hr />
                </div>
              ))}
            </div>
          ) : searchQuery ? (
            <div className="Result-No-found">No landmark found</div>
          ) : null}
        </div>
        <img
          id="directionIcon"
          title="Direction"
          className="icon1 "
          src={Dir}
          alt="Logo"
          style={{ height: "50px", width: "50px" }}
          onClick={direction}
        />
        <img
          id="Search_by_nearIcon"
          title="Search near by"
          className="icon2 "
          src={Loc}
          alt="Logo"
          style={{ height: "50px", width: "50px" }}
          onClick={search_by_near}
        />
        <img
          id="Search_by_serviceIcon"
          title="Search by service area"
          className="icon3 "
          src={Ser}
          alt="Logo"
          style={{ height: "50px", width: "50px" }}
          onClick={search_by_service}
        />

        <div id="closeNav1">
          <button className="" onClick={closeNav1}>
            <img
              id="closeIcon"
              title="collapse"
              className="icon4 icon"
              src={Arow}
              alt="Logo"
              style={{ height: "50px", width: "50px" }}
            />
          </button>
        </div>

        <div id="openNav1" style={{ display: "none" }}>
          <button className="" onClick={openNav1}>
            <img
              title="collapse"
              className="icon4 icon"
              src={Right_aroow}
              alt="Logo"
              style={{ height: "50px", width: "50px" }}
            />
          </button>
        </div>
      </div>
      <div></div>

      <button className="GetAPP">
        {" "}
        <a href="https://play.google.com/store/apps/details?id=net.gamiya.addis_abeba_ethiopia.offline.navigation">
          Get the App
        </a>
      </button>
      <button className="login">
        {" "}
        <a href="./login">Login</a>
      </button>

      <div className="location_share">
        <div id="coordinates"></div>
      </div>

      <div className="bottomright">
        <a href=""> Copyright © 2023 SSGI</a>
      </div>
      <div id="scaleline-metric"></div>
      <div id="search_by_near"></div>
      <div id="search_by_service"></div>
      <div id="FullScreen"></div>
      <div id="layerSwitcher"></div>
      <div id="Location_click"></div>
      
    </Fragment>
  );
}
export default Testmap;
