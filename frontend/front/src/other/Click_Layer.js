var vectorLayer = new VectorLayer({
    source: new VectorSource(),
  });
  
  // Add the vector layer to the map
  map.addLayer(vectorLayer);
      map.on("singleclick", function (evt) {
        var view = map.getView();
  
        var view = map.getView();
  
        // Define the zoom level and duration of the animation
        var zoom = view.getZoom() + 1;
        var duration = 500; // in milliseconds
      
        // Get the clicked coordinates and transform them to the map's projection
        var clickedCoord = evt.coordinate;
        var transformedCoord = proj.transform(
          clickedCoord,
          "EPSG:3857",
          view.getProjection()
        );
      
        // Animate the view to the new zoom level centered on the clicked point
        view.animate({
          center: transformedCoord,
          duration: duration,
        });
      
        var coord = evt.coordinate;
        var transformed_coordinate = proj.transform(
          coord,
          "EPSG:900913",
          "EPSG:4326"
        );
        console.log(transformed_coordinate);
  
        var share = '&nbsp&nbsp&nbsp&nbsp<button class="share">Share</button>';
        // alert("Clicked coordinates: " + transformed_coordinate[1]+ "" + ", " + "" + transformed_coordinate[0] );
        document.getElementById("coordinates").innerHTML =
          "<br>" + transformed_coordinate + share
          ;
        var pointFeature = new Feature({
          geometry: new Point(transformed_coordinate),
        });
        // Create a point feature at the transformed coordinates and add it to the vector layer
    var pointFeature = new Feature({
      geometry: new Point(transformedCoord),
      //icon
      style: new Style({
        image: new Icon({
          src: 'dir.png',
          size: [32, 32],
          anchor: [0.5, 1],
        }),
      }),
    });
    vectorLayer.getSource().addFeature(pointFeature);
      });