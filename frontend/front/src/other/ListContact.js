import React, { Fragment, useState,useEffect } from "react";
import EditContact from "./editContact";


const ListContact = ()=>{

    const [api,setApi] = useState([]);
    //delete contact function
     const deleteContact=async id =>{
        try {
            const deleteContact = await fetch(`http://localhost:4001/api/${id}`,{
                method:"DELETE"
            });
            //show update after delete
            setApi(api.filter(api=>api.contact_id !==id))

        } catch (err) {
            console.error(err.messagege)
            
        }
     };
// edit contact 


    const getContact = async()=>{
        try {
            
            const response= await fetch("http://localhost:4001/api");
            const jsonData = await response.json();
            console.log(jsonData);
            setApi(jsonData);
        } catch (err) {
            // console.error(err.message);
            
        }
    }
    useEffect(()=>{
        getContact();
    },[]);
    console.log(api);
    return (
    <Fragment>
         
        <center>
        <h1 className="text-center mt-5">List contact</h1>
        
        {" "}
        <div className="portal">
        <table class="list table text-center">
            <thead>
  <tr>
    {/* <th>id</th> */}
    <th>Name</th> 
    <th>Phone Number</th> 
   
    <th>Edit</th>
    <th>Delete</th>
  </tr>
  </thead>
  <tbody>
 {api.map(api=>(
    <tr key={api.contact_id}>
        {/* <td>{api.contact_id}</td> */}
        <td>{api.name}</td>
        <td>{api.phone_number}</td>
        <td><EditContact api={api}/></td>
        
        
        <td><button className="btn btn-danger" onClick={()=>deleteContact(api.contact_id)}>Delete</button> </td>
        
    </tr>
 ))}
  </tbody>
</table>
</div>
</center>

    </Fragment>
    );
};

export default ListContact;