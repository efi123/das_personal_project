   //pointerMove
   map.on("pointermove", function (evt) {
    if (evt.dragging) {
      return;
    }
    const data = wmsLayer.getData(evt.pixel);
    const hit = data && data[3] > 0; // transparent pixels have zero for data[3]
    map.getTargetElement().style.cursor = hit ? "pointer" : "";
  });