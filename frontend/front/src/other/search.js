import React, { useState } from 'react';
import axios from 'axios';
import { FaSearch, FaBars } from 'react-icons/fa';
import './css/map.css'


function Search() {
  const [searchQuery, setSearchQuery] = useState('');
  const [searchResults, setSearchResults] = useState([]);
  const [isLoading, setIsLoading] = useState(false);

  async function fetchSearchResults(query) {
    if (!query) {
      // query is empty, do nothing
      return;
    }
    setIsLoading(true); // Set loading state to true
    try {
      const response = await axios.get(`http://localhost:4001/landmark?q=${query}`);
      setSearchResults([]); // Clear the search results before setting new ones

      setSearchResults(response.data);
    } catch (error) {
      console.error(error);
    } finally {
      setIsLoading(false); // Set loading state to false
    }
  }

  function handleSearchChange(e) {
    const input = e.target.value;
    setSearchQuery(input);
    fetchSearchResults(input);
  }

  return (

    <div>

        
        <div>
          
        <div className="input-box-container">
  <div className="search-box">
    <div className="sidebar-icon">
      <FaBars  />
    </div>
    <input
      type="text"
      placeholder="Search"
      value={searchQuery}
      onChange={handleSearchChange}
    />
  </div>
</div>
   
    </div>
     
      {isLoading ? (
        <div>Loading...</div>
        ) : searchResults.length > 0 ? (
          <div className="search-results-container">
          {searchResults.map((result) => (
            <div key={result.name_eng}>
              <a href={`${result.name_eng}`}>
                {result.name_eng}&nbsp;/&nbsp;{result.name_amh}
              </a>
              <br />
              <br />
            </div>
          ))}
        </div>
        
        ) :searchQuery ?  (
          <div>No matches found.</div>
        ) : null}
      </div>
  );
}

export default Search;
