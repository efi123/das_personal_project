const mousePositionControl = new MousePosition({
    className: "custom-mouse-position",
    target: document.getElementById("projection1"),
    coordinateFormat: createStringXY(10),
    projection: "EPSG:4326",
    undefinedHTML: "&nbsp;",
  });