import React,{ Fragment, useState} from "react";



const InputContact=()=>{

    const [name,setName]= useState("");
    const [phone_number,setPhone_number]= useState("");

    const onSubmitForm= async e =>{
        e.preventDefault();
        try {
           const body={ name,phone_number};
           
           const response =  await fetch("http://localhost:4001/api",{
            method:"POST",
            headers:{"Content-Type":"application/json"},
            body:JSON.stringify(body)
           }
           );
        
        } catch (err) {
            console.error(err.message)
            
        }
    }
    return(
        
        <Fragment>
 
<center>
        <h1 className=" text-center mt-5"> Input New Contact</h1>
        <div className=" form-group text-center list ">
        <div className="portal">
        <form className="form-horizontal " onSubmit={onSubmitForm}>
            
        <div className="form-group input-field">
     
    


     <input  id="focusedInput" type="text" value={name}onChange={e=>setName(e.target.value)} placeholder="Enter Name"required/><br/><br/>
    <input id="focusedInput" type="number"value={phone_number}onChange={e=>setPhone_number(e.target.value)}  placeholder="Enter Phone Number"required/><br/>
       <br/><button className="btn btn-success">Add</button>
     
      </div>
          
        </form>
        </div>
        </div>
        </center>
        </Fragment>
      
    )
};
 export default InputContact;