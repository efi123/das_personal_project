  //search onsubmit
  const [name, setName] = useState([]);
  const onSubmitForm = async (e) => {
    e.preventDefault();
    try {
      const response = await fetch(`http://localhost:4001/api/?name=${name}`);
      const parseResponse = await response.json();

      const formattedResponse = JSON.stringify(parseResponse, null, 2);
      console.log(formattedResponse);
      document.getElementById(
        "Search_Click"
      ).innerHTML = `<pre>${formattedResponse}</pre>`;
    } catch (err) {
      console.error(err.message);
    }
  };