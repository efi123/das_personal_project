 //add map/Layer
 const mapRef = useRef(null);
 useEffect(() => {
   const map = new Map({
     controls: defaultControls().extend([mousePositionControl]),
     target: mapRef.current,
     layers: [
       new TileLayer({
         source: new OSM(),
         title: "osm",
         visible: false,
       }),
       new Tile({
         source: new XYZ({
           url: "https://{a-c}.tile.openstreetmap.fr/hot/{z}/{x}/{y}.png",
         }),
         title: "Humanitarian",
         visible: false,
       }),
       new Tile({
         // A layer must have a title to appear in the layerswitcher
         title: "Satelite",
         // Again set this layer as a base layer

         visible: false,
         source: new Stamen({
           layer: "terrain",
         }),
       }),
       new TileLayer({
         source: new TileWMS({
           url: "http://localhost:8080/geoserver/Das_Final/wms",
           params: { LAYERS: "Das_Final:Das_Final", TILED: true },
           serverType: "geoserver",

           type: "base",
           visible: true,
           transition: 0,
         }),
         title: "Bishoftu",
       }),
     ],

     view: new View({
       center: fromLonLat([38.965577549276546, 8.75323422342312]),

       zoom: 13,
       // minZoom: 5,
       // maxZoom: 19,
     }),
   });
   var view = new View({
     projection: "EPSG:4326",
     center: [78.0, 23.0],
     zoom: 9,
   });
   const wmsSource = new TileWMS({
     url: "http://localhost:8080/geoserver/Das_Final/wms",
     params: { LAYERS: "Das_Final:Das_Final", TILED: true },
     serverType: "geoserver",
     crossOrigin: "anonymous",
   });

   const wmsLayer = new TileLayer({
     source: wmsSource,
   });

   const view1 = new View({
     center: [0, 0],
     zoom: 1,
   });

   const map1 = new Map({
     layers: [wmsLayer],
     target: "map",
     view: view,
   });