import React, { Fragment ,useState} from "react";


const EditContact = ({ api })=>{
    const[name,setName]=useState(api.name);
    const[phone_number,setPhone_number]=useState(api.phone_number);
    // console.log(api);
    const updateContact=async (e) =>{
        e.preventDefault();

        try {
            const body={phone_number,name};
            const response = await fetch(`http://localhost:4001/api/${api.contact_id}`,{
            method:"PUT",
            headers:{"content-Type":"application/json"},
            body:JSON.stringify(body)
            
        }
        
            )
            
        } catch (err) {
            console.error(err.message);
            
        }
    };

   
    return (
    <Fragment>
        <button type="button"
         class="btn btn-warning" 
        data-toggle="modal" 
        data-target={`#id${api.contact_id}`}>
          Edit
        </button>
        

        <div class="modal" id={`id${api.contact_id}`}>
          <div class="modal-dialog">
            <div class="modal-content">
        
              
              <div class="modal-header">
                <h4 class="modal-title">Modal Heading</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
              </div>
        
            
              <div class="modal-body">
             <label>Name:</label><input type="text" value={phone_number}onChange={e=>setPhone_number(e.target.value)}/><br/><br/>
            <label>Phone No:</label><input type="Number" value={phone_number}onChange={e=>setPhone_number(e.target.value)}/>
              </div>
        
              
              <div class="modal-footer">
              <button type="button" 
              className="btn btn-warning" 
              data-dismiss="modal"
              onClick={e=>updateContact(e)}
              >Edit
              </button>

          <button type="button" className="btn btn-danger" data-dismiss="modal">
              close
              </button> </div>
        
            </div>
          </div>
        </div>
        </Fragment>
    );
};
export default EditContact;