import "./side.css";
import { fromLonLat } from "ol/proj";
import "ol-layerswitcher/dist/ol-layerswitcher.css";
import { Map, View } from "ol";
import TileLayer from "ol/layer/Tile";
import TileWMS from "ol/source/TileWMS";
import { Fragment } from "react";
import { OSM } from "ol/source";
function Landmark() {
    var map = new Map({
        target: 'map',
        layers: [
           
              new TileLayer({
                source: new TileWMS({
                    url: "http://localhost:8080/geoserver/Das_Final/wms",
                    params: { LAYERS: "Das_Final:landmark_4326_v5", TILED: true },
                    serverType: "geoserver",
                    visible: true,
                    transition: 0,
              }),
            })
          ],
        view: new View({
            center: fromLonLat([37.41, 8.82], 'EPSG:3857'),
            zoom: 6
        })
      });
    
    const wmsLayer = new TileLayer({
        source: new TileWMS({
            url: "http://localhost:8080/geoserver/Das_Final/wms",
            params: { LAYERS: "Das_Final:landmark_4326_v5", TILED: true },
            serverType: "geoserver",

            type: "base",
            visible: true,
            transition: 0,
      }),
    });
    map.addLayer(wmsLayer)
    return (
    <Fragment>
      
      <div id="map" class="map"></div>

    </Fragment>
  );
}
export default Landmark;
