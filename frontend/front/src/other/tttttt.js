import './map.css'
import './side.css'
import './ol.css'
import Dir from './dir.jpg'
import Right_aroow from './right_arrow.png'
import Loc from './location.jpg'
import Ser from './service.jpg'
import Arow from './arrow.jfif'
import 'ol-layerswitcher/dist/ol-layerswitcher.css';
import LayerSwitcher from 'ol-layerswitcher';
import OSM from "ol/source/OSM";
import * as proj from 'ol/proj';
import Feature from 'ol/Feature';
import Point from 'ol/geom/Point';
import Overlay from 'ol/Overlay'
import { useEffect, useRef, useState } from 'react';
import { Map, View } from 'ol';
import TileLayer from 'ol/layer/Tile';
import {ScaleLine, defaults as defaultControls,MousePosition,FullScreen} from 'ol/control.js';
import TileWMS from 'ol/source/TileWMS';
import ImageWMS from 'ol/source/ImageWMS';
import vectorLayer from 'ol/layer/Vector'
import Group from 'ol/layer/Group';
import Style from 'ol/style/Style';
import Icon from 'ol/style/Icon';
import { Vector } from 'ol/source'
import { get } from 'ol/proj'
import { fromLonLat } from 'ol/proj';
import * as bootstrap from 'bootstrap';
import Tile from 'ol/layer/Tile';
import { XYZ } from 'ol/source'
import {Stamen} from 'ol/source'
import Geolocation from 'ol/Geolocation.js';
import {createStringXY} from 'ol/coordinate.js';
import {Circle as CircleStyle, Fill, Stroke} from 'ol/style.js';
import {Vector as VectorSource} from 'ol/source.js';
import {Vector as VectorLayer} from 'ol/layer.js';
import { Fragment } from 'react'
function Testmap() {
  const [name,setName]= useState([]);
  console.log(name);



  //search onsubmit
  const onSubmitForm = async e => {    
    e.preventDefault();
    try {
      const response = await fetch(`http://localhost:4001/api/?name=${name}`);
      const parseResponse = await response.json();
      console.log(parseResponse);
      const formattedResponse = JSON.stringify(parseResponse, null, 2);
      console.log(formattedResponse);
      document.getElementById("data").innerHTML = `<pre>${formattedResponse}</pre>`;
  
      // create a new map object centered at the location obtained from the API response
      const map = new Map(document.getElementById("map-container"), {
        center: {
          lat: parseResponse.latitude,
          lng: parseResponse.longitude
        },
        zoom: 10
      });
  
    // Create a feature with a point geometry
var feature = new Feature({
  center: {
    lat: parseResponse.latitude,
    lng: parseResponse.longitude
  },});

// Set the style of the marker
feature.setStyle(new Style({
  image: new Icon({
    anchor: [0.5, 1],
    src: 'https://openlayers.org/en/latest/examples/data/icon.png',
  }),
}));

// Create a vector layer and add the feature to it
var vectorLayer = new Vector({
  source: new Vector({
    features: [feature],
  }),
});
console.log(vectorLayer);
// Add the vector layer to the map
map.addLayer(vectorLayer);

  
    } catch (err) {
      console.error(err.message);
    }
  };
  
  
  
  
  //Mouse movememnt
  const mousePositionControl = new MousePosition({
    className: 'custom-mouse-position',
     target: document.getElementById('projection1'),
    coordinateFormat: createStringXY(10),
    projection: 'EPSG:4326',
    undefinedHTML: '&nbsp;'
  });
 

  //add map/Layer
  const mapRef = useRef(null);
  useEffect(() => {
    const map = new Map({
      controls: defaultControls().extend([mousePositionControl]),
      target: mapRef.current,
      layers: [
        new TileLayer({
          source: new OSM(),
          title:'osm',
          visible: false,
         }),
          new Tile({
            source: new XYZ({
              url: 'https://{a-c}.tile.openstreetmap.fr/hot/{z}/{x}/{y}.png',
            }),
            title: 'Humanitarian',
            visible: false,
          }),
          new Tile({
            // A layer must have a title to appear in the layerswitcher
            title: 'Satelite',
            // Again set this layer as a base layer
           
            visible: false,
            source: new Stamen({
                layer: 'terrain'
            })
          }),
        new TileLayer({
          source: new TileWMS({
            
            url: 'http://localhost:8080/geoserver/Das_Final/wms',
            params: { 'LAYERS': 'Das_Final:Das_Final', 'TILED': true },
            serverType: 'geoserver',
           
            type: 'base',
            visible: true,
            transition: 0
            }),
            title:'Bishoftu',
          } )
        
      ],
      
      view: new View({
        center: fromLonLat([38.965577549276546, 8.75323422342312]),
        
        zoom: 12,
    // minZoom: 5,
    // maxZoom: 19,
      }),
    });
    var view = new View({
      projection: 'EPSG:4326',
             center: [78.0,23.0],
              zoom: 5,
          
            });
            const wmsSource = new TileWMS({
              url: 'http://localhost:8080/geoserver/Das_Final/wms',
              params: { 'LAYERS': 'Das_Final:Das_Final', 'TILED': true },
              serverType: 'geoserver',
              crossOrigin: 'anonymous',
            });
            
            const wmsLayer = new TileLayer({
              source: wmsSource,
            });
            
            const view1 = new View({
              center: [0, 0],
              zoom: 1,
            });
            
            const map1 = new Map({
              layers: [wmsLayer],
              target: 'map',
              view: view,
            });
   
  
   
            var container = document.getElementById('popup1');
            var content = document.getElementById('popup-content1');
            var overlay = new Overlay({
              element: container,
              autoPan: true,
              autoPanAnimation: {
                duration: 250
              }
            });
   //layerSwitcher
    var layerSwitcher = new LayerSwitcher({
      reverse: true,
      activationMode:"click",
      groupSelectStyle: 'children'
     
    });
    map.addControl(layerSwitcher);

map.on('click', function(evtt) {
  var feature = map.forEachFeatureAtPixel(evtt.pixel, function(feature, layer) {
    return feature;
  });
  
  if (feature) {
    console.log(feature.getProperties()); // log feature properties to console
  } else {
    console.log('No feature found');
  }
});




 //geolocation
//  const geolocation = new Geolocation({
//   // enableHighAccuracy must be set to true to have the heading value.
//   trackingOptions: {
//     enableHighAccuracy: true,
//   },
//   projection: view.getProjection(),
// });

// function el(id) {
//   return document.getElementById(id);
// }
// el('track').addEventListener('change', function () {
//   geolocation.setTracking(this.checked);
// });
// // update the HTML page when the position changes.
// geolocation.on('change', function () {
//   el('accuracy').innerText = geolocation.getAccuracy() + ' [m]';
//  console.log(geolocation);
// });
// // handle geolocation error.
// geolocation.on('error', function (error) {
//   const info = document.getElementById('info1');
//   info.innerHTML = error.message;
//   info.style.display = '';
// });
// const accuracyFeature = new Feature();
// geolocation.on('change:accuracyGeometry', function () {
//   accuracyFeature.setGeometry(geolocation.getAccuracyGeometry());
// });
// const positionFeature = new Feature();
// positionFeature.setStyle(
//   new Style({
//     image: new CircleStyle({
//       radius: 6,
//       fill: new Fill({
//         color: '#3399CC',
//       }),
//       stroke: new Stroke({
//         color: '#fff',
//         width: 2,
//       }),
//     }),
//   })
// );
// geolocation.on('change:position', function () {
//   const coordinates = geolocation.getPosition();
//   positionFeature.setGeometry(coordinates ? new Point(coordinates) : null);
// });
// new VectorLayer({
//   map: map,
//   source: new VectorSource({
//     features: [accuracyFeature, positionFeature],
//   }),
// });
  
      
//full screen
    var full_sc = new FullScreen({label:'F'});
    map.addControl(full_sc);



    // map.on('singleclick', function (evt) {
    //   document.getElementById('info').innerHTML = '';
    //   const viewResolution = /** @type {number} */ (view1.getResolution());
    //   const url = wmsSource.getFeatureInfoUrl(
    //     evt.coordinate,
    //     viewResolution,
    //     'EPSG:3857',
    //     {'INFO_FORMAT': 'text/html'}
    //   );
    //   if (url) {
    //     fetch(url)
        
    //       .then((response) => response.text())
    //       .then((html) => {
    //         document.getElementById('info').innerHTML = html;
    //       });
    //   }
      
    // });
    
    map.on('pointermove', function (evt) {
      if (evt.dragging) {
        return;
      }
      const data = wmsLayer.getData(evt.pixel);
      const hit = data && data[3] > 0; // transparent pixels have zero for data[3]
      map.getTargetElement().style.cursor = hit ? 'pointer' : '';
    });

 
 
  
    // var overlay = new Overlay({
    //   element: document.createElement('div'),
    //   positioning: 'bottom-center'
    // });
    // var iconStyle = new Style({
    //   image: new Icon({
    //     anchor: [0.5, 1],
    //     src: './das.jpg'
    //   })
      
    // });
    // map.on('click', function(evt) {
    //   overlay.setPosition(evt.coordinate);
    //   overlay.getElement().innerHTML = '';
    //   var iconFeature = new Feature(new Point(evt.coordinate));
    //   iconFeature.setStyle(iconStyle);
    //   var iconSource = new Vector({
    //     features: [iconFeature]
    //   });
    //   var iconLayer = new Vector({
    //     source: iconSource
    //   });
    //   map.addLayer(iconLayer);
    // });
    
    // map.addOverlay(overlay);
    
    map.on('click', function(evt) {
      overlay.setPosition(evt.coordinate);
    });
    

// Add a click event listener to the map
map.on('singleclick', function(evt) {
  var coord = evt.coordinate;
  var transformed_coordinate = proj.transform(coord, "EPSG:900913", "EPSG:4326");
  console.log(transformed_coordinate);
 

  var test =('&nbsp&nbsp&nbsp&nbsp<button class="share">Share</button>')
  // alert("Clicked coordinates: " + transformed_coordinate[1]+ "" + ", " + "" + transformed_coordinate[0] );
  document.getElementById("coordinates").innerHTML = '<br>'+ transformed_coordinate  + test;
  var pointFeature = new Feature({
    geometry: new Point(transformed_coordinate)
    
  });
 
  });


  //zoom Icon
  map.on('zoomend', map, function() {
    var zoomInfo = 'Zoom level=' + map.getZoom() + '/' + (map.numZoomLevels + 1);
    document.getElementById('shortdesc').innerHTML = 'Show a Simple OSM Map --- ' + zoomInfo;
  })



 // Create the scalelines within the two div elements
// Popup showing the position the user clicked
// const closer = document.getElementById('popup-closer');
// closer.onclick = function () {
//   popup.setPosition(null);
//   closer.blur();
//   return true;};

  
// const popup = new Overlay({
//   element: document.getElementById('popup'),});
// map.addOverlay(popup);
// const element = popup.getElement();
// map.on('click', function (evt) {
//   const coordinate = evt.coordinate;
//   var transformed_coordinate = proj.transform(coordinate, "EPSG:900913", "EPSG:4326");
  
//   popup.setPosition(coordinate);
//   let popover = bootstrap.Popover.getInstance(element);
//   if (popover) {
//     popover.dispose();
//   }
//   popover = new bootstrap.Popover(element, {
//     animation: false,
//     container: element,
//     content: '<p>The location you clicked was:</p><code>' + transformed_coordinate + '</code>',
//     html: true,
//     placement: 'bottom',
//     title: 'Welcome to SSGI',
//   });
  
//   popover.show();});



//Scale view
 var scaleLineMetric = new ScaleLine({
  units: ['metric'],
  target: document.getElementById("scaleline-metric")});
map.addControl(scaleLineMetric);
  });


   function direction (){
    var test ='Search Location'
    document.getElementById("SearchLocation").innerHTML = test;
    document.getElementById("SearchLocation").style.background ='rgb(230, 223, 223)';  
    document.getElementById("SearchLocation").style.fontSize ='11'; 
    document.getElementById("SearchLocation").style.border ='solid 1px';  
    document.getElementById("SearchLocation").style.borderRadius ='10px'; 
    document.getElementById("SearchLocation").style.width ='250px'; 
    document.getElementById("SearchLocation").style.height ='100%'; 
    document.getElementById("SearchLocation").style.top ='0'; 
    document.getElementById("SearchLocation").style.left ='0'; 
    document.getElementById("direction").style.display = "none";
    document.getElementById("Search_near").style.display = "none";
    document.getElementById("Search_service").style.display = "none";
    document.getElementById("search").style.display = "none";
   document.getElementById("closeNav1").style.display = "none";
   document.getElementById("openNav1").style.display = "none";
  };
   function search_by_near (){
    var search_by_near ='search_by_near'
    document.getElementById("search_by_near").innerHTML = search_by_near;
    document.getElementById("search_by_near").style.background ='rgb(230, 223, 223)';  
    document.getElementById("search_by_near").style.fontSize ='11'; 
    document.getElementById("search_by_near").style.border ='solid 1px';  
    document.getElementById("search_by_near").style.borderRadius ='10px'; 
    document.getElementById("search_by_near").style.width ='250px'; 
    document.getElementById("search_by_near").style.height ='100%'; 
    document.getElementById("search_by_near").style.top ='0'; 
    document.getElementById("search_by_near").style.left ='0'; 
    document.getElementById("direction").style.display = "none";
    document.getElementById("Search_near").style.display = "none";
    document.getElementById("Search_service").style.display = "none";
    document.getElementById("search").style.display = "none";
   document.getElementById("closeNav1").style.display = "none";
   document.getElementById("openNav1").style.display = "none";  
   };
   function search_by_service (){
    var search_by_service ='search_by_near'
    document.getElementById("search_by_service").innerHTML = search_by_service;
    document.getElementById("search_by_service").style.background ='rgb(230, 223, 223)';  
    document.getElementById("search_by_service").style.fontSize ='11'; 
    document.getElementById("search_by_service").style.border ='solid 1px';  
    document.getElementById("search_by_service").style.borderRadius ='10px'; 
    document.getElementById("search_by_service").style.width ='250px'; 
    document.getElementById("search_by_service").style.height ='100%'; 
    document.getElementById("search_by_service").style.top ='0'; 
    document.getElementById("search_by_service").style.left ='0'; 
    document.getElementById("direction").style.display = "none";
    document.getElementById("Search_near").style.display = "none";
    document.getElementById("Search_service").style.display = "none";
    document.getElementById("search").style.display = "none";
   document.getElementById("closeNav1").style.display = "none";
   document.getElementById("openNav1").style.display = "none"; 
   };
   function openNav() {
    document.getElementById("mySidebar").style.width = "250px";
    document.getElementById("main").style.marginLeft = "250px";
    
  };
  function closeNav1() {
    document.getElementById("direction").style.display = "none";
    document.getElementById("Search_near").style.display = "none";
    document.getElementById("Search_service").style.display = "none";
    document.getElementById("search").style.display = "none";
   document.getElementById("closeNav1").style.display = "none";
   document.getElementById("openNav1").style.display = "inline";
   document.getElementById("main").style.display = "none";
  }
  function openNav1() {  
    document.getElementById("direction").style.display = "inline";
    document.getElementById("Search_near").style.display = "inline";
    document.getElementById("Search_service").style.display = "inline";
    document.getElementById("search").style.display = "inline";
    document.getElementById("closeNav1").style.display = "inline";
    document.getElementById("openNav1").style.display = "none";
    document.getElementById("main").style.display = "inline";

  }
  function closeNav() {
    document.getElementById("mySidebar").style.width = "0";
    document.getElementById("main").style.marginLeft= "0";
  }
  return (
 <Fragment>
           
 
  <div id="map-container" ref={mapRef} style={{ width: '100%', height: '710px' }} />
  <div className="container-input ">
  <div id="mySidebar" className="sidebar">
  <a href="javascript:void(0)" className="closebtn" onClick={closeNav} >×</a>
  <a href="#">SSGi</a>
  <a href="#">Services</a>
  <a href="#">Add place</a>
  <a href="#">Add Busieness</a>
</div>

<div id="main">
  <button className="openbtn"onClick={openNav}>☰ </button>  
</div>
 <form onSubmit={onSubmitForm} id="search">
  <input class="form-control" id="search"type="text"placeholder="Place,name address" onChange={e=>setName(e.target.value)}/>
  
</form>

<img id="direction" title="Direction" className='icon1 icon' src={Dir} alt="Logo" style={{height: '35px',width:'35px'}} onClick={direction}/>
<img id="Search_near"title="Search near by"  className='icon2 icon'src={Loc} alt="Logo" style={{height: '35px',width:'35px'}}onClick={search_by_near}/>
<img id="Search_service"title="Search by service area"  className='icon3 icon' src={Ser} alt="Logo" style={{height: '35px',width:'35px'}}onClick={search_by_service}/>

<div id="closeNav1" >
  <button className=""onClick={closeNav1}><img id="closeIcon"title="collapse"  className='icon4 icon'  src={Arow} alt="Logo" style={{height: '20px',width:'20px'}}/>
 </button>  
</div>


<div id="openNav1" style={{display:'none'}} >
<button className=""onClick={openNav1}><img title="collapse"  className='icon4 icon'  src={Right_aroow} alt="Logo" style={{height: '10px',width:'10px'}}/>
 </button> 
</div>



</div>




<button className='GetAPP'> <a href='https://play.google.com/store/apps/details?id=net.gamiya.addis_abeba_ethiopia.offline.navigation'>Get the App</a></button>
    <button className='login'> <a href='./login'>Login</a></button>
<div className='location_share'>
<div id='coordinates'></div>

</div>
  
   
    <div className="bottomright">
   <a href=""> Copyright © 2023 SSGI</a>
    </div>
    <div id="scaleline-metric"></div>
   
   
{/*    
    <div id="popup" className="ol-popup">
      <a href="#" id="popup-closer" className="ol-popup-closer"></a>
      <div id="popup-content"></div>
    </div>
    */}
	 
	 <div id="info"></div>
   
   <div id='projection1'></div>
  




      <div id='SearchLocation' >

      </div>
      <div id='search_by_near'></div>
      <div id='search_by_service'></div>
     <div id='address'></div>
    

     {/* <div id="info1" style={{display:'none'}}></div>
    <label for="track">
      track position
      <input id="track" type="checkbox"/>
    </label>
    <p>
      position accuracy : <code id="accuracy"></code>
    
    </p> */}
   <div id='data'></div>
    </Fragment>
         
    );

};
export default Testmap;