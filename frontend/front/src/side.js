import React, { useState } from "react";
import ReactDOM from "react-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {faBars,faTimes,faCogs,faTable,faList,faUser
} from "@fortawesome/free-solid-svg-icons";

import "./css/side.css";
import Logo from './images/das-removebg.png'

function Side() {
  const [isOpen, setIsOpen] = useState(false);

  const handleTrigger = () => setIsOpen(!isOpen);

 
}
export default Side;