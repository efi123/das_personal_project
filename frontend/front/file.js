
import React from 'react';
import AddData from './AddData';

function App() {
  const apiUrl = 'http://localhost:3001';
  return (
    <div>
      <h1>Add Data</h1>
      <AddData apiUrl={apiUrl} />
    </div>
  );
}

export default App;