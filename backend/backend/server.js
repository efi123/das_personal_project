const express = require('express');
const cors = require('cors');
const app = express();
const landmarkRoutes = require('./Routes/Route');
require('dotenv').config();
const sequelize = require('./database'); // Assuming you have set up the database connection

app.use(express.json());

// Enable CORS
app.use(cors());

// Routes
app.use('/api', landmarkRoutes);

// Start the server
const PORT = process.env.PORT || 4001;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});

// Test the database connection
(async () => {
  try {
    await sequelize.authenticate();
    console.log('Database connection established successfully');
  } catch (error) {
    console.error('Unable to connect to the database:', error);
  }
})();
