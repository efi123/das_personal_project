const stringSimilarity = require('string-similarity');

const Landmark = require('../Models/Model');

exports.createLandmark = async (req, res) => {
  try {
    const { name, description } = req.body;

    // Create a new landmark
    const landmark = await Landmark.create({ name, description });

    res.status(201).json({ message: 'Landmark created successfully', landmark });
  } catch (error) {
    res.status(500).json({ message: 'Failed to create landmark', error });
  }
};

// exports.getAllLandmarks = async (req, res) => {
//   try {
//     const landmarks = await Landmark.findAll();
//     const landmarkData = landmarks.map((landmark) => {
//       return {
//         gid: landmark.gid,
//         nameEng: landmark.name_eng,
//         nameAmh: landmark.name_amh,
//         region: landmark.region,
//       };
//     });
//     res.json(landmarkData);
//   } catch (error) {
//     console.error('Error retrieving landmarks:', error);
//     res.status(500).json({ error: 'An error occurred while retrieving landmarks' });
//   }
// };

exports.getAllLandmarksCatagory = async (req, res) => {
  try {
    const funType = req.query.q; // Get the value of the fun_type query parameter
console.log(funType);
    const landmarks = await Landmark.findAll();
    const filteredLandmarks = landmarks.filter(landmark => landmark.fun_type == funType);

    console.log(filteredLandmarks);
    const landmarkData = filteredLandmarks.map((landmark) => {
      return {
        gid: landmark.gid,
        nameEng: landmark.name_eng,
        nameAmh: landmark.name_amh,
        region: landmark.region,
        zone:landmark.zone,
        woreda:landmark.woreda,
        kebele:landmark.kebele,
        city_name:landmark.city_name,
        lat:landmark.bldg_lat,
        long:landmark.bldg_long,

      };
    });
    res.json(landmarkData);
  } catch (error) {
    console.error('Error retrieving landmarks:', error);
    res.status(500).json({ error: 'An error occurred while retrieving landmarks' });
  }
};


exports.getAllLandmarks = async (req, res) => {
  try {
    const funType = req.query.q; // Get the value of the fun_type query parameter
console.log(funType);
    const landmarks = await Landmark.findAll();
    const filteredLandmarks = landmarks.filter(landmark => {
      const similarity = stringSimilarity.compareTwoStrings(landmark.name_eng.toLowerCase(), funType.toLowerCase());
      return similarity >= 0.45; // Filter if the similarity is greater than or equal to 40%
    });
    console.log(filteredLandmarks);
    const landmarkData = filteredLandmarks.map((landmark) => {
      return {
      
        nameEng: landmark.name_eng,
       
      };
    });
    res.json(landmarkData);
  } catch (error) {
    console.error('Error retrieving landmarks:', error);
    res.status(500).json({ error: 'An error occurred while retrieving landmarks' });
  }
};
