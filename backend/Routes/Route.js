const express = require('express');
const router = express.Router();
const landmarkController = require('../Controllers/Controller');

// Create a new landmark
router.post('/landmarks', landmarkController.createLandmark);

// Get all landmarks
router.get('/landmarks', landmarkController.getAllLandmarksCatagory);

router.get('/landmarks1', landmarkController.getAllLandmarks);


module.exports = router;
