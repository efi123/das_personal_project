const { Sequelize, DataTypes } = require('sequelize');
const sequelize = require('../database'); // Assuming you have set up the database connection

const Landmark = sequelize.define('landmark_v7.2', {
  gid: {
    type: DataTypes.INTEGER,
    allowNull: false,
    primaryKey: true,
  },
  name_eng: {
    type: DataTypes.STRING,
  },
  name_amh: {
    type: DataTypes.STRING,
  },
  region: {
    type: DataTypes.STRING,
  },
  fun_type: {
    type: DataTypes.STRING,
  },
  zone: {
    type: DataTypes.STRING,
  },
  woreda: {
    type: DataTypes.STRING,
  },
  kebele: {
    type: DataTypes.STRING,
  },
  city_name: {
    type: DataTypes.STRING,
  },
  bldg_lat:{
    type: DataTypes.NUMBER,
  },
  bldg_long:{
    type: DataTypes.NUMBER,
  },
}, {
  tableName: 'landmark_v7.2',
  timestamps: false,
  primaryKey: false,
});

module.exports = Landmark;
